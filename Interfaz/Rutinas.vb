﻿Module Rutinas
    Public cn As New ADODB.Connection
    Public rs As New ADODB.Recordset
    Public dt As New DataTable
    Public da As New OleDb.OleDbDataAdapter
    Public ci As String
    Public pass As String
    Public rol As String
    Public sql As String
    Public fullFields As Boolean
    Public validInput As Boolean
    Public rec As Integer
    Public Sub display(ByRef cb As ComboBox)
        rs.Open(sql, cn)
        dt.Clear()
        da.Fill(dt, rs)
        If rs.State = 1 Then
            rs.Close()
        End If
        cb.DataSource = dt
    End Sub
    Public Sub display(ByRef dgv As DataGridView)
        rs.Open(sql, cn)
        dt.Clear()
        da.Fill(dt, rs)
        If rs.State = 1 Then
            rs.Close()
        End If
        dgv.DataSource = dt
    End Sub
    Public Sub fill(ByRef cb As ComboBox, column As String, table As String)
        sql = "SELECT DISTINCT " & column & " FROM " & table
        display(cb)
    End Sub
    Public Sub fill(ByRef cb As ComboBox)
        cb.Items.Add("Gerente")
        cb.Items.Add("Administrativo")
        cb.Items.Add("Cliente")
        cb.Items.Add("Asesor")
        cb.Items.Add("Conductor")
    End Sub
    Public Sub addItem(ByRef cb As ComboBox, ByRef item As TextBox)
        If Not cb.Items.Contains(item.Text) Then
            cb.Items.Add(item.Text)
            item.Clear()
        Else
            MsgBox("Item duplicado")
        End If
    End Sub
    Public Sub updateId(ByRef cb As ComboBox, type As String)
        sql = "SELECT id_recipiente FROM Recipiente WHERE tipo = '" & type & "'"
        display(cb)
    End Sub
    Function noRegistrado(ced As String) As Boolean
        sql = "SELECT * FROM Usuario WHERE CI = '" & ced & "'"
        rs.Open(sql, cn)
        rec = rs.RecordCount
        If rs.State = 1 Then
            rs.Close()
        End If
        If rec = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
End Module
