﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAgendarTransporte
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.txtDestino = New System.Windows.Forms.TextBox()
        Me.txtOrigen = New System.Windows.Forms.TextBox()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.lblDestino = New System.Windows.Forms.Label()
        Me.lblOrigen = New System.Windows.Forms.Label()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.lblHoraInicio = New System.Windows.Forms.Label()
        Me.txtHoraInicio = New System.Windows.Forms.TextBox()
        Me.txtMinutoInicio = New System.Windows.Forms.TextBox()
        Me.txtMinutoFin = New System.Windows.Forms.TextBox()
        Me.txtHoraFin = New System.Windows.Forms.TextBox()
        Me.lblHoraFin = New System.Windows.Forms.Label()
        Me.lblColumnInicio = New System.Windows.Forms.Label()
        Me.lblColumnFin = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(70, 291)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 23
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(460, 291)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 22
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'txtDestino
        '
        Me.txtDestino.Location = New System.Drawing.Point(309, 121)
        Me.txtDestino.Name = "txtDestino"
        Me.txtDestino.Size = New System.Drawing.Size(200, 20)
        Me.txtDestino.TabIndex = 19
        '
        'txtOrigen
        '
        Me.txtOrigen.Location = New System.Drawing.Point(309, 86)
        Me.txtOrigen.Name = "txtOrigen"
        Me.txtOrigen.Size = New System.Drawing.Size(200, 20)
        Me.txtOrigen.TabIndex = 18
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(98, 155)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(46, 19)
        Me.lblFecha.TabIndex = 15
        Me.lblFecha.Text = "Fecha"
        '
        'lblDestino
        '
        Me.lblDestino.AutoSize = True
        Me.lblDestino.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDestino.Location = New System.Drawing.Point(98, 120)
        Me.lblDestino.Name = "lblDestino"
        Me.lblDestino.Size = New System.Drawing.Size(55, 19)
        Me.lblDestino.TabIndex = 14
        Me.lblDestino.Text = "Destino"
        '
        'lblOrigen
        '
        Me.lblOrigen.AutoSize = True
        Me.lblOrigen.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOrigen.Location = New System.Drawing.Point(98, 85)
        Me.lblOrigen.Name = "lblOrigen"
        Me.lblOrigen.Size = New System.Drawing.Size(50, 19)
        Me.lblOrigen.TabIndex = 13
        Me.lblOrigen.Text = "Origen"
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(309, 154)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(200, 20)
        Me.dtpFecha.TabIndex = 27
        '
        'lblHoraInicio
        '
        Me.lblHoraInicio.AutoSize = True
        Me.lblHoraInicio.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHoraInicio.Location = New System.Drawing.Point(98, 186)
        Me.lblHoraInicio.Name = "lblHoraInicio"
        Me.lblHoraInicio.Size = New System.Drawing.Size(94, 19)
        Me.lblHoraInicio.TabIndex = 28
        Me.lblHoraInicio.Text = "Hora de inicio"
        '
        'txtHoraInicio
        '
        Me.txtHoraInicio.Location = New System.Drawing.Point(309, 187)
        Me.txtHoraInicio.Name = "txtHoraInicio"
        Me.txtHoraInicio.Size = New System.Drawing.Size(48, 20)
        Me.txtHoraInicio.TabIndex = 29
        '
        'txtMinutoInicio
        '
        Me.txtMinutoInicio.Location = New System.Drawing.Point(378, 187)
        Me.txtMinutoInicio.Name = "txtMinutoInicio"
        Me.txtMinutoInicio.Size = New System.Drawing.Size(48, 20)
        Me.txtMinutoInicio.TabIndex = 30
        '
        'txtMinutoFin
        '
        Me.txtMinutoFin.Location = New System.Drawing.Point(378, 219)
        Me.txtMinutoFin.Name = "txtMinutoFin"
        Me.txtMinutoFin.Size = New System.Drawing.Size(48, 20)
        Me.txtMinutoFin.TabIndex = 33
        '
        'txtHoraFin
        '
        Me.txtHoraFin.Location = New System.Drawing.Point(309, 219)
        Me.txtHoraFin.Name = "txtHoraFin"
        Me.txtHoraFin.Size = New System.Drawing.Size(48, 20)
        Me.txtHoraFin.TabIndex = 32
        '
        'lblHoraFin
        '
        Me.lblHoraFin.AutoSize = True
        Me.lblHoraFin.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHoraFin.Location = New System.Drawing.Point(98, 218)
        Me.lblHoraFin.Name = "lblHoraFin"
        Me.lblHoraFin.Size = New System.Drawing.Size(77, 19)
        Me.lblHoraFin.TabIndex = 31
        Me.lblHoraFin.Text = "Hora de fin"
        '
        'lblColumnInicio
        '
        Me.lblColumnInicio.AutoSize = True
        Me.lblColumnInicio.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnInicio.Location = New System.Drawing.Point(363, 188)
        Me.lblColumnInicio.Name = "lblColumnInicio"
        Me.lblColumnInicio.Size = New System.Drawing.Size(14, 19)
        Me.lblColumnInicio.TabIndex = 34
        Me.lblColumnInicio.Text = ":"
        '
        'lblColumnFin
        '
        Me.lblColumnFin.AutoSize = True
        Me.lblColumnFin.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblColumnFin.Location = New System.Drawing.Point(363, 220)
        Me.lblColumnFin.Name = "lblColumnFin"
        Me.lblColumnFin.Size = New System.Drawing.Size(14, 19)
        Me.lblColumnFin.TabIndex = 35
        Me.lblColumnFin.Text = ":"
        '
        'frmAgendarTransporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.lblColumnFin)
        Me.Controls.Add(Me.lblColumnInicio)
        Me.Controls.Add(Me.txtMinutoFin)
        Me.Controls.Add(Me.txtHoraFin)
        Me.Controls.Add(Me.lblHoraFin)
        Me.Controls.Add(Me.txtMinutoInicio)
        Me.Controls.Add(Me.txtHoraInicio)
        Me.Controls.Add(Me.lblHoraInicio)
        Me.Controls.Add(Me.dtpFecha)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.txtDestino)
        Me.Controls.Add(Me.txtOrigen)
        Me.Controls.Add(Me.lblFecha)
        Me.Controls.Add(Me.lblDestino)
        Me.Controls.Add(Me.lblOrigen)
        Me.Name = "frmAgendarTransporte"
        Me.Text = "Agendar Transporte"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents btnIngresar As System.Windows.Forms.Button
    Friend WithEvents txtDestino As System.Windows.Forms.TextBox
    Friend WithEvents txtOrigen As System.Windows.Forms.TextBox
    Friend WithEvents lblFecha As System.Windows.Forms.Label
    Friend WithEvents lblDestino As System.Windows.Forms.Label
    Friend WithEvents lblOrigen As System.Windows.Forms.Label
    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents lblHoraInicio As Label
    Friend WithEvents txtHoraInicio As TextBox
    Friend WithEvents txtMinutoInicio As TextBox
    Friend WithEvents txtMinutoFin As TextBox
    Friend WithEvents txtHoraFin As TextBox
    Friend WithEvents lblHoraFin As Label
    Friend WithEvents lblColumnInicio As Label
    Friend WithEvents lblColumnFin As Label
End Class
