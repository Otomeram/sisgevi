﻿Public Class frmAgendarTransporte
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuConductor.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = txtDestino.Text.Any And txtHoraFin.Text.Any And txtHoraInicio.Text.Any And txtMinutoFin.Text.Any And txtMinutoInicio.Text.Any And txtOrigen.Text.Any
        validInput = IsNumeric(txtHoraFin.Text) And IsNumeric(txtHoraInicio.Text) And IsNumeric(txtMinutoFin.Text) And IsNumeric(txtMinutoInicio.Text)
        If fullFields And validInput Then
            sql = "INSERT INTO Transporte Values('" & ci & "', '" & dtpFecha.Value.ToString("dd/MM/yyyy") & "', '" & txtHoraInicio.Text & ":" & txtMinutoInicio.Text & "', '" & txtHoraFin.Text & ":" & txtMinutoFin.Text & "', '" & txtOrigen.Text & "', '" & txtDestino.Text & "')"
            cn.Execute(sql)
            MsgBox("El transporte fue agendado con éxito")
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("Uno o más campos numéricos contienen caracteres inválidos")
        End If
    End Sub
End Class