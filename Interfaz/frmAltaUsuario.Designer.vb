﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmAltasUsuarios
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblNPuerta = New System.Windows.Forms.Label()
        Me.lblEsquina = New System.Windows.Forms.Label()
        Me.lblCalle = New System.Windows.Forms.Label()
        Me.lblTel = New System.Windows.Forms.Label()
        Me.lblCi = New System.Windows.Forms.Label()
        Me.lblApellido = New System.Windows.Forms.Label()
        Me.lblNombre = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.txtApellido = New System.Windows.Forms.TextBox()
        Me.txtCI = New System.Windows.Forms.TextBox()
        Me.txtCalle = New System.Windows.Forms.TextBox()
        Me.txtEsquina = New System.Windows.Forms.TextBox()
        Me.txtNPuerta = New System.Windows.Forms.TextBox()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.txtTel = New System.Windows.Forms.TextBox()
        Me.cbTel = New System.Windows.Forms.ComboBox()
        Me.btnAddTel = New System.Windows.Forms.Button()
        Me.lblFechaDeNacimiento = New System.Windows.Forms.Label()
        Me.dtpFechaDeNacimiento = New System.Windows.Forms.DateTimePicker()
        Me.lblRol = New System.Windows.Forms.Label()
        Me.cbRol = New System.Windows.Forms.ComboBox()
        Me.btnRemoveTel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblNPuerta
        '
        Me.lblNPuerta.AutoSize = True
        Me.lblNPuerta.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNPuerta.Location = New System.Drawing.Point(116, 202)
        Me.lblNPuerta.Name = "lblNPuerta"
        Me.lblNPuerta.Size = New System.Drawing.Size(89, 19)
        Me.lblNPuerta.TabIndex = 13
        Me.lblNPuerta.Text = "N de Puerta"
        '
        'lblEsquina
        '
        Me.lblEsquina.AutoSize = True
        Me.lblEsquina.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEsquina.Location = New System.Drawing.Point(116, 166)
        Me.lblEsquina.Name = "lblEsquina"
        Me.lblEsquina.Size = New System.Drawing.Size(56, 19)
        Me.lblEsquina.TabIndex = 12
        Me.lblEsquina.Text = "Esquina"
        '
        'lblCalle
        '
        Me.lblCalle.AutoSize = True
        Me.lblCalle.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCalle.Location = New System.Drawing.Point(116, 131)
        Me.lblCalle.Name = "lblCalle"
        Me.lblCalle.Size = New System.Drawing.Size(40, 19)
        Me.lblCalle.TabIndex = 11
        Me.lblCalle.Text = "Calle"
        '
        'lblTel
        '
        Me.lblTel.AutoSize = True
        Me.lblTel.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTel.Location = New System.Drawing.Point(116, 101)
        Me.lblTel.Name = "lblTel"
        Me.lblTel.Size = New System.Drawing.Size(77, 19)
        Me.lblTel.TabIndex = 10
        Me.lblTel.Text = "Teléfono(s)"
        '
        'lblCi
        '
        Me.lblCi.AutoSize = True
        Me.lblCi.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCi.Location = New System.Drawing.Point(117, 71)
        Me.lblCi.Name = "lblCi"
        Me.lblCi.Size = New System.Drawing.Size(25, 19)
        Me.lblCi.TabIndex = 9
        Me.lblCi.Text = "CI"
        '
        'lblApellido
        '
        Me.lblApellido.AutoSize = True
        Me.lblApellido.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblApellido.Location = New System.Drawing.Point(116, 39)
        Me.lblApellido.Name = "lblApellido"
        Me.lblApellido.Size = New System.Drawing.Size(60, 19)
        Me.lblApellido.TabIndex = 8
        Me.lblApellido.Text = "Apellido"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.Location = New System.Drawing.Point(116, 9)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(60, 19)
        Me.lblNombre.TabIndex = 7
        Me.lblNombre.Text = "Nombre"
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(359, 8)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(100, 20)
        Me.txtNombre.TabIndex = 14
        '
        'txtApellido
        '
        Me.txtApellido.Location = New System.Drawing.Point(359, 40)
        Me.txtApellido.Name = "txtApellido"
        Me.txtApellido.Size = New System.Drawing.Size(100, 20)
        Me.txtApellido.TabIndex = 15
        '
        'txtCI
        '
        Me.txtCI.Location = New System.Drawing.Point(360, 72)
        Me.txtCI.Name = "txtCI"
        Me.txtCI.Size = New System.Drawing.Size(100, 20)
        Me.txtCI.TabIndex = 16
        '
        'txtCalle
        '
        Me.txtCalle.Location = New System.Drawing.Point(359, 131)
        Me.txtCalle.Name = "txtCalle"
        Me.txtCalle.Size = New System.Drawing.Size(100, 20)
        Me.txtCalle.TabIndex = 17
        '
        'txtEsquina
        '
        Me.txtEsquina.Location = New System.Drawing.Point(359, 165)
        Me.txtEsquina.Name = "txtEsquina"
        Me.txtEsquina.Size = New System.Drawing.Size(100, 20)
        Me.txtEsquina.TabIndex = 18
        '
        'txtNPuerta
        '
        Me.txtNPuerta.Location = New System.Drawing.Point(359, 200)
        Me.txtNPuerta.Name = "txtNPuerta"
        Me.txtNPuerta.Size = New System.Drawing.Size(100, 20)
        Me.txtNPuerta.TabIndex = 19
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(454, 307)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 20
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(90, 307)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 21
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'txtTel
        '
        Me.txtTel.Location = New System.Drawing.Point(359, 101)
        Me.txtTel.Name = "txtTel"
        Me.txtTel.Size = New System.Drawing.Size(100, 20)
        Me.txtTel.TabIndex = 23
        '
        'cbTel
        '
        Me.cbTel.FormattingEnabled = True
        Me.cbTel.Location = New System.Drawing.Point(477, 100)
        Me.cbTel.Name = "cbTel"
        Me.cbTel.Size = New System.Drawing.Size(100, 21)
        Me.cbTel.TabIndex = 24
        '
        'btnAddTel
        '
        Me.btnAddTel.Location = New System.Drawing.Point(333, 101)
        Me.btnAddTel.Name = "btnAddTel"
        Me.btnAddTel.Size = New System.Drawing.Size(20, 20)
        Me.btnAddTel.TabIndex = 25
        Me.btnAddTel.Text = "+"
        Me.btnAddTel.UseVisualStyleBackColor = True
        '
        'lblFechaDeNacimiento
        '
        Me.lblFechaDeNacimiento.AutoSize = True
        Me.lblFechaDeNacimiento.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.lblFechaDeNacimiento.Location = New System.Drawing.Point(117, 235)
        Me.lblFechaDeNacimiento.Name = "lblFechaDeNacimiento"
        Me.lblFechaDeNacimiento.Size = New System.Drawing.Size(133, 19)
        Me.lblFechaDeNacimiento.TabIndex = 26
        Me.lblFechaDeNacimiento.Text = "Fecha de nacimiento"
        '
        'dtpFechaDeNacimiento
        '
        Me.dtpFechaDeNacimiento.Location = New System.Drawing.Point(359, 235)
        Me.dtpFechaDeNacimiento.Name = "dtpFechaDeNacimiento"
        Me.dtpFechaDeNacimiento.Size = New System.Drawing.Size(209, 20)
        Me.dtpFechaDeNacimiento.TabIndex = 27
        '
        'lblRol
        '
        Me.lblRol.AutoSize = True
        Me.lblRol.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.lblRol.Location = New System.Drawing.Point(117, 264)
        Me.lblRol.Name = "lblRol"
        Me.lblRol.Size = New System.Drawing.Size(30, 19)
        Me.lblRol.TabIndex = 28
        Me.lblRol.Text = "Rol"
        '
        'cbRol
        '
        Me.cbRol.FormattingEnabled = True
        Me.cbRol.Location = New System.Drawing.Point(359, 264)
        Me.cbRol.Name = "cbRol"
        Me.cbRol.Size = New System.Drawing.Size(100, 21)
        Me.cbRol.TabIndex = 29
        '
        'btnRemoveTel
        '
        Me.btnRemoveTel.Location = New System.Drawing.Point(583, 101)
        Me.btnRemoveTel.Name = "btnRemoveTel"
        Me.btnRemoveTel.Size = New System.Drawing.Size(20, 20)
        Me.btnRemoveTel.TabIndex = 54
        Me.btnRemoveTel.Text = "-"
        Me.btnRemoveTel.UseVisualStyleBackColor = True
        '
        'frmAltasUsuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnRemoveTel)
        Me.Controls.Add(Me.cbRol)
        Me.Controls.Add(Me.lblRol)
        Me.Controls.Add(Me.dtpFechaDeNacimiento)
        Me.Controls.Add(Me.lblFechaDeNacimiento)
        Me.Controls.Add(Me.btnAddTel)
        Me.Controls.Add(Me.cbTel)
        Me.Controls.Add(Me.txtTel)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.txtNPuerta)
        Me.Controls.Add(Me.txtEsquina)
        Me.Controls.Add(Me.txtCalle)
        Me.Controls.Add(Me.txtCI)
        Me.Controls.Add(Me.txtApellido)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lblNPuerta)
        Me.Controls.Add(Me.lblEsquina)
        Me.Controls.Add(Me.lblCalle)
        Me.Controls.Add(Me.lblTel)
        Me.Controls.Add(Me.lblCi)
        Me.Controls.Add(Me.lblApellido)
        Me.Controls.Add(Me.lblNombre)
        Me.Name = "frmAltasUsuarios"
        Me.Text = "Alta de usuario"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblNPuerta As System.Windows.Forms.Label
    Friend WithEvents lblEsquina As System.Windows.Forms.Label
    Friend WithEvents lblCalle As System.Windows.Forms.Label
    Friend WithEvents lblTel As System.Windows.Forms.Label
    Friend WithEvents lblCi As System.Windows.Forms.Label
    Friend WithEvents lblApellido As System.Windows.Forms.Label
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents txtApellido As System.Windows.Forms.TextBox
    Friend WithEvents txtCI As System.Windows.Forms.TextBox
    Friend WithEvents txtCalle As System.Windows.Forms.TextBox
    Friend WithEvents txtEsquina As System.Windows.Forms.TextBox
    Friend WithEvents txtNPuerta As System.Windows.Forms.TextBox
    Friend WithEvents btnIngresar As System.Windows.Forms.Button
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents txtTel As TextBox
    Friend WithEvents cbTel As ComboBox
    Friend WithEvents btnAddTel As Button
    Friend WithEvents lblFechaDeNacimiento As Label
    Friend WithEvents dtpFechaDeNacimiento As DateTimePicker
    Friend WithEvents lblRol As Label
    Friend WithEvents cbRol As ComboBox
    Friend WithEvents btnRemoveTel As Button
End Class
