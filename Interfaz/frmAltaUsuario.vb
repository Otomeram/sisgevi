﻿Public Class frmAltasUsuarios
    Dim mayorDe18 As Boolean
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmUsuarios.Show()
        Dispose()
    End Sub
    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = txtApellido.Text.Any And txtCalle.Text.Any And txtCI.Text.Any And txtEsquina.Text.Any And txtNombre.Text.Any And txtNPuerta.Text.Any
        validInput = IsNumeric(txtCI.Text) And IsNumeric(txtNPuerta.Text)
        If fullFields And validInput Then
            mayorDe18 = DateDiff(DateInterval.Year, dtpFechaDeNacimiento.Value, Date.Today) >= 18
            If mayorDe18 Then
                If noRegistrado(txtCI.Text) Then
                    sql = "INSERT INTO Usuario VALUES('" & txtCI.Text & "', '" & cbRol.SelectedItem.ToString() & "', '" & txtNombre.Text & "', '" & txtApellido.Text & "', '" & dtpFechaDeNacimiento.Value.ToString("dd/MM/yyyy") & "', '" & txtCalle.Text & "', '" & txtEsquina.Text & "', '" & txtNPuerta.Text & "')"
                    cn.Execute(sql)
                    For Each tel In cbTel.Items
                        sql = "INSERT INTO TelefonoUsuario VALUES('" & txtCI.Text & "', '" & tel & "')"
                        cn.Execute(sql)
                    Next
                    MsgBox("El usuario fue registrado con éxito")
                Else
                    MsgBox("El usuario ya está registrado en el sistema")
                End If
            Else
                MsgBox("Para registrar a un nuevo usuario este debe ser mayor de 18 años")
            End If
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("Uno o más campos numéricos contienen caracteres inválidos")
        End If
    End Sub
    Private Sub btnAddTel_Click(sender As Object, e As EventArgs) Handles btnAddTel.Click
        If IsNumeric(txtTel.Text) Then
            addItem(cbTel, txtTel)
        Else
            MsgBox("Caracteres inválidos")
        End If
    End Sub
    Private Sub frmAltasUsuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbRol)
        If Not ci = "12345678" Then
            cbRol.Items.Remove("Gerente")
        End If
    End Sub
    Private Sub btnRemoveTel_Click(sender As Object, e As EventArgs) Handles btnRemoveTel.Click
        cbTel.Items.Remove(cbTel.SelectedItem)
    End Sub
End Class