﻿Public Class frmBorradoProceso
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmBorradosAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub frmBorradoProceso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbProcesos, "nombre", "Proceso")
    End Sub

    Private Sub btnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click
        sql = "DELETE * FROM Proceso WHERE nombre = '" & cbProcesos.Text & "'"
        cn.Execute(sql)
        cbProcesos.Items.Remove(cbProcesos.Text)
        MsgBox("El proceso fue removido exitosamente")
    End Sub
End Class