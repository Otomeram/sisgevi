﻿Public Class frmBorradoProducto
    Dim ultimaFecha As Date
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmBorradosAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click
        sql = "SELECT fecha_inicio FROM Va WHERE id_recipiente = '" & cbIdRecipiente.SelectedItem.ToString & "'"
        rs.Open(sql, cn)
        ultimaFecha = findLastDate()
        If rs.State = 1 Then
            rs.Close()
        End If
        sql = "DELETE FROM Va WHERE id_recipiente = '" & cbIdRecipiente.SelectedItem.ToString & "' AND fecha_inicio = '" & ultimaFecha & "'"
    End Sub

    Private Sub frmBorradoProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub

    Private Function findLastDate() As Date
        ultimaFecha = Date.Parse("01/01/2000")
        Do Until rs.EOF
            If ultimaFecha < Date.Parse(rs("fecha_inicio").Value) Then
                ultimaFecha = Date.Parse(rs("fecha_inicio").Value)
            End If
            rs.MoveNext()
        Loop
        Return ultimaFecha
    End Function
End Class