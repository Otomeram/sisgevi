﻿Public Class frmBorradoRecipiente
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmBorradosAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnBorrar_Click(sender As Object, e As EventArgs) Handles btnBorrar.Click
        sql = "DELETE FROM Recipiente WHERE id_recipiente = '" & cbIdRecipiente.Text & "'"
        cn.Execute(sql)
        cbIdRecipiente.Items.Remove(cbIdRecipiente.Text)
        MsgBox("El recipiente fue borrado exitosamente")
    End Sub

    Private Sub frmBorradoRecipiente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class