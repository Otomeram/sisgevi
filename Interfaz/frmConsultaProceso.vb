﻿
Public Class frmConsultaProceso
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Select Case rol
            Case "Administrativo"
                frmConsultasAdministrativo.Show()
            Case "Asesor"
                frmConsultasAsesor.Show()
        End Select
        Dispose()
    End Sub

    Private Sub frmConsultaProceso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbProcesos, "nombre", "Proceso")
    End Sub
End Class