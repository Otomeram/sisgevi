﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaProducto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblLote = New System.Windows.Forms.Label()
        Me.lblTipoDeUva = New System.Windows.Forms.Label()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.cbUvas = New System.Windows.Forms.ComboBox()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.dgvResultado = New System.Windows.Forms.DataGridView()
        Me.txtLote = New System.Windows.Forms.TextBox()
        Me.lblTipoDeRecipiente = New System.Windows.Forms.Label()
        Me.cbTipoDeRecipiente = New System.Windows.Forms.ComboBox()
        Me.chkTipoDeRecipiente = New System.Windows.Forms.CheckBox()
        Me.chkIdRecipiente = New System.Windows.Forms.CheckBox()
        Me.chkUvas = New System.Windows.Forms.CheckBox()
        CType(Me.dgvResultado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblLote
        '
        Me.lblLote.AutoSize = True
        Me.lblLote.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLote.Location = New System.Drawing.Point(178, 126)
        Me.lblLote.Name = "lblLote"
        Me.lblLote.Size = New System.Drawing.Size(37, 19)
        Me.lblLote.TabIndex = 0
        Me.lblLote.Text = "Lote"
        '
        'lblTipoDeUva
        '
        Me.lblTipoDeUva.AutoSize = True
        Me.lblTipoDeUva.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeUva.Location = New System.Drawing.Point(178, 161)
        Me.lblTipoDeUva.Name = "lblTipoDeUva"
        Me.lblTipoDeUva.Size = New System.Drawing.Size(80, 19)
        Me.lblTipoDeUva.TabIndex = 1
        Me.lblTipoDeUva.Text = "Tipo de uva"
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(178, 92)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(84, 19)
        Me.lblIdRecipiente.TabIndex = 2
        Me.lblIdRecipiente.Text = "Id recipiente"
        '
        'cbUvas
        '
        Me.cbUvas.FormattingEnabled = True
        Me.cbUvas.Location = New System.Drawing.Point(317, 162)
        Me.cbUvas.Name = "cbUvas"
        Me.cbUvas.Size = New System.Drawing.Size(121, 21)
        Me.cbUvas.TabIndex = 5
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(318, 93)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbIdRecipiente.TabIndex = 6
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(478, 388)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 9
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(74, 388)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 10
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'dgvResultado
        '
        Me.dgvResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResultado.Location = New System.Drawing.Point(12, 199)
        Me.dgvResultado.Name = "dgvResultado"
        Me.dgvResultado.Size = New System.Drawing.Size(593, 183)
        Me.dgvResultado.TabIndex = 15
        '
        'txtLote
        '
        Me.txtLote.Location = New System.Drawing.Point(318, 127)
        Me.txtLote.Name = "txtLote"
        Me.txtLote.Size = New System.Drawing.Size(120, 20)
        Me.txtLote.TabIndex = 16
        '
        'lblTipoDeRecipiente
        '
        Me.lblTipoDeRecipiente.AutoSize = True
        Me.lblTipoDeRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente.Location = New System.Drawing.Point(178, 57)
        Me.lblTipoDeRecipiente.Name = "lblTipoDeRecipiente"
        Me.lblTipoDeRecipiente.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente.TabIndex = 17
        Me.lblTipoDeRecipiente.Text = "Tipo de recipiente"
        '
        'cbTipoDeRecipiente
        '
        Me.cbTipoDeRecipiente.FormattingEnabled = True
        Me.cbTipoDeRecipiente.Location = New System.Drawing.Point(318, 58)
        Me.cbTipoDeRecipiente.Name = "cbTipoDeRecipiente"
        Me.cbTipoDeRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbTipoDeRecipiente.TabIndex = 18
        '
        'chkTipoDeRecipiente
        '
        Me.chkTipoDeRecipiente.AutoSize = True
        Me.chkTipoDeRecipiente.Location = New System.Drawing.Point(157, 62)
        Me.chkTipoDeRecipiente.Name = "chkTipoDeRecipiente"
        Me.chkTipoDeRecipiente.Size = New System.Drawing.Size(15, 14)
        Me.chkTipoDeRecipiente.TabIndex = 19
        Me.chkTipoDeRecipiente.UseVisualStyleBackColor = True
        '
        'chkIdRecipiente
        '
        Me.chkIdRecipiente.AutoSize = True
        Me.chkIdRecipiente.Location = New System.Drawing.Point(157, 97)
        Me.chkIdRecipiente.Name = "chkIdRecipiente"
        Me.chkIdRecipiente.Size = New System.Drawing.Size(15, 14)
        Me.chkIdRecipiente.TabIndex = 20
        Me.chkIdRecipiente.UseVisualStyleBackColor = True
        '
        'chkUvas
        '
        Me.chkUvas.AutoSize = True
        Me.chkUvas.Location = New System.Drawing.Point(157, 166)
        Me.chkUvas.Name = "chkUvas"
        Me.chkUvas.Size = New System.Drawing.Size(15, 14)
        Me.chkUvas.TabIndex = 22
        Me.chkUvas.UseVisualStyleBackColor = True
        '
        'frmConsultaProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 438)
        Me.Controls.Add(Me.chkUvas)
        Me.Controls.Add(Me.chkIdRecipiente)
        Me.Controls.Add(Me.chkTipoDeRecipiente)
        Me.Controls.Add(Me.cbTipoDeRecipiente)
        Me.Controls.Add(Me.lblTipoDeRecipiente)
        Me.Controls.Add(Me.txtLote)
        Me.Controls.Add(Me.dgvResultado)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.cbUvas)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.lblTipoDeUva)
        Me.Controls.Add(Me.lblLote)
        Me.Name = "frmConsultaProducto"
        Me.Text = "Consulta de producto"
        CType(Me.dgvResultado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblLote As System.Windows.Forms.Label
    Friend WithEvents lblTipoDeUva As System.Windows.Forms.Label
    Friend WithEvents lblIdRecipiente As System.Windows.Forms.Label
    Friend WithEvents cbUvas As System.Windows.Forms.ComboBox
    Friend WithEvents cbIdRecipiente As System.Windows.Forms.ComboBox
    Friend WithEvents btnIngresar As System.Windows.Forms.Button
    Friend WithEvents btnVolver As System.Windows.Forms.Button
    Friend WithEvents dgvResultado As System.Windows.Forms.DataGridView
    Friend WithEvents txtLote As TextBox
    Friend WithEvents lblTipoDeRecipiente As Label
    Friend WithEvents cbTipoDeRecipiente As ComboBox
    Friend WithEvents chkTipoDeRecipiente As CheckBox
    Friend WithEvents chkIdRecipiente As CheckBox
    Friend WithEvents chkUvas As CheckBox
End Class
