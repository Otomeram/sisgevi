﻿
Public Class frmConsultaProducto
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Select Case rol
            Case "Administrativo"
                frmConsultasAdministrativo.Show()
            Case "Asesor"
                frmConsultasAsesor.Show()
            Case "Gerente"
                frmMenuGerente.Show()
            Case "Cliente"
                frmMenuCliente.Show()
        End Select
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        sql = "SELECT * FROM Va"
        Dim notNullParameters As Boolean = chkIdRecipiente.Checked Or chkTipoDeRecipiente.Checked Or chkUvas.Checked Or txtLote.Text.Any
        If notNullParameters Then
            sql += " WHERE"
        End If
        If chkIdRecipiente.Checked Then
            sql += " id_recipiente = '" & cbIdRecipiente.Text & "'"
            If chkTipoDeRecipiente.Checked Or chkUvas.Checked Or txtLote.Text.Any Then
                sql += " AND"
            End If
        End If
        If chkTipoDeRecipiente.Checked Then
            sql += " tipo = '" & cbTipoDeRecipiente.Text & "'"
            If chkUvas.Checked Or txtLote.Text.Any Then
                sql += " AND"
            End If
        End If
        If chkUvas.Checked Then
            sql += " nombre = '" & cbUvas.Text & "'"
            If txtLote.Text.Any Then
                sql += " AND"
            End If
        End If
        If txtLote.Text.Any And IsNumeric(txtLote.Text) Then
            sql += "lote = '" & txtLote.Text & "'"
        ElseIf Not IsNumeric(txtLote.Text) Then
            MsgBox("El lote ingresado contiene caracteres inválidos")
            Exit Sub
        End If
        display(dgvResultado)
    End Sub

    Private Sub frmConsultaProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbUvas, "nombre", "Producto")
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class

