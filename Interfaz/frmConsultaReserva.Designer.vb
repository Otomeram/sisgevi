﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultaReserva
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.dgvResultado = New System.Windows.Forms.DataGridView()
        Me.txtLote = New System.Windows.Forms.TextBox()
        Me.lblLote = New System.Windows.Forms.Label()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.txtCICliente = New System.Windows.Forms.TextBox()
        Me.lblCICliente = New System.Windows.Forms.Label()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.lblFecha = New System.Windows.Forms.Label()
        Me.chkFecha = New System.Windows.Forms.CheckBox()
        CType(Me.dgvResultado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtpFecha
        '
        Me.dtpFecha.Location = New System.Drawing.Point(291, 98)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(200, 20)
        Me.dtpFecha.TabIndex = 28
        '
        'dgvResultado
        '
        Me.dgvResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvResultado.Location = New System.Drawing.Point(85, 139)
        Me.dgvResultado.Name = "dgvResultado"
        Me.dgvResultado.Size = New System.Drawing.Size(446, 176)
        Me.dgvResultado.TabIndex = 27
        '
        'txtLote
        '
        Me.txtLote.Location = New System.Drawing.Point(292, 61)
        Me.txtLote.Name = "txtLote"
        Me.txtLote.Size = New System.Drawing.Size(123, 20)
        Me.txtLote.TabIndex = 26
        '
        'lblLote
        '
        Me.lblLote.AutoSize = True
        Me.lblLote.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLote.Location = New System.Drawing.Point(81, 61)
        Me.lblLote.Name = "lblLote"
        Me.lblLote.Size = New System.Drawing.Size(37, 19)
        Me.lblLote.TabIndex = 25
        Me.lblLote.Text = "Lote"
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(73, 321)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'txtCICliente
        '
        Me.txtCICliente.Location = New System.Drawing.Point(292, 26)
        Me.txtCICliente.Name = "txtCICliente"
        Me.txtCICliente.Size = New System.Drawing.Size(124, 20)
        Me.txtCICliente.TabIndex = 23
        '
        'lblCICliente
        '
        Me.lblCICliente.AutoSize = True
        Me.lblCICliente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCICliente.Location = New System.Drawing.Point(81, 27)
        Me.lblCICliente.Name = "lblCICliente"
        Me.lblCICliente.Size = New System.Drawing.Size(89, 19)
        Me.lblCICliente.TabIndex = 22
        Me.lblCICliente.Text = "CI del cliente"
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(469, 321)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 21
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'lblFecha
        '
        Me.lblFecha.AutoSize = True
        Me.lblFecha.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFecha.Location = New System.Drawing.Point(81, 99)
        Me.lblFecha.Name = "lblFecha"
        Me.lblFecha.Size = New System.Drawing.Size(46, 19)
        Me.lblFecha.TabIndex = 16
        Me.lblFecha.Text = "Fecha"
        '
        'chkFecha
        '
        Me.chkFecha.AutoSize = True
        Me.chkFecha.Location = New System.Drawing.Point(60, 103)
        Me.chkFecha.Name = "chkFecha"
        Me.chkFecha.Size = New System.Drawing.Size(15, 14)
        Me.chkFecha.TabIndex = 29
        Me.chkFecha.UseVisualStyleBackColor = True
        '
        'frmConsultaReserva
        '
        Me.AcceptButton = Me.btnIngresar
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.chkFecha)
        Me.Controls.Add(Me.dtpFecha)
        Me.Controls.Add(Me.dgvResultado)
        Me.Controls.Add(Me.txtLote)
        Me.Controls.Add(Me.lblLote)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.txtCICliente)
        Me.Controls.Add(Me.lblCICliente)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.lblFecha)
        Me.Name = "frmConsultaReserva"
        Me.Text = "Consultas de Reservas"
        CType(Me.dgvResultado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dtpFecha As DateTimePicker
    Friend WithEvents dgvResultado As DataGridView
    Friend WithEvents txtLote As TextBox
    Friend WithEvents lblLote As Label
    Friend WithEvents btnVolver As Button
    Friend WithEvents txtCICliente As TextBox
    Friend WithEvents lblCICliente As Label
    Friend WithEvents btnIngresar As Button
    Friend WithEvents lblFecha As Label
    Friend WithEvents chkFecha As CheckBox
End Class
