﻿Public Class frmConsultaReserva
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        sql = "SELECT * FROM Reserva"
        If txtCICliente.Text.Any Or txtLote.Text.Any Or chkFecha.Checked Then
            sql += " WHERE"
        End If
        If txtCICliente.Text.Any And IsNumeric(txtCICliente.Text) Then
            sql += " ci = '" & txtCICliente.Text & "'"
            If txtLote.Text.Any Or chkFecha.Checked Then
                sql += " AND"
            End If
        ElseIf Not IsNumeric(txtCICliente.Text) Then
            MsgBox("El CI ingresado contiene caracteres inválidos")
            Exit Sub
        End If
        If txtLote.Text.Any And IsNumeric(txtLote.Text) Then
            sql += " lote = '" & txtLote.Text & "'"
            If chkFecha.Checked Then
                sql += " AND"
            End If
        ElseIf Not IsNumeric(txtLote.Text) Then
            MsgBox("El lote ingresado contiene caracteres inválidos")
            Exit Sub
        End If
        If chkFecha.Checked Then
            sql += " fecha = '" & dtpFecha.Value.ToString("dd/MM/yyyy") & "'"
        End If
        display(dgvResultado)
    End Sub
End Class