﻿Public Class frmConsultaTransporte
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmConsultasAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = chkFecha.Checked Or txtCIConductor.Text.Any Or txtDestino.Text.Any Or txtOrigen.Text.Any
        sql = "SELECT * FROM " & tipo()
        If fullFields Then
            sql += " WHERE"
        End If
        If chkFecha.Checked Then
            sql += " fecha = '" & dtpFecha.Value.ToString("dd/MM/yyyy") & "'"
            If txtCIConductor.Text.Any Or txtDestino.Text.Any Or txtOrigen.Text.Any Then
                sql += " AND"
            End If
        End If
        If txtCIConductor.Text.Any And IsNumeric(txtCIConductor.Text) Then
            sql += " ci = '" & txtCIConductor.Text & "'"
            If txtOrigen.Text.Any Or txtDestino.Text.Any Then
                sql += " AND"
            End If
        ElseIf Not IsNumeric(txtCIConductor.Text) Then
            MsgBox("La cédula ingresada contiene caracteres inválidos")
            Exit Sub
        End If
        If txtDestino.Text.Any Then
            sql += " nombre = '" & txtDestino.Text & "'"
            If txtOrigen.Text.Any Then
                sql += " AND"
            End If
        End If
        If txtOrigen.Text.Any Then
            sql += " lote = '" & txtOrigen.Text & "'"
        End If
        display(dgvResultado)
    End Sub

    Private Sub frmConsultaTransporte_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbTipo.Items.Add("Agendados")
        cbTipo.Items.Add("Ingresados")
    End Sub

    Private Function tipo() As String
        Select Case cbTipo.Text
            Case "Agendados"
                Return "Transporte"
            Case "Ingresados"
                Return "Transporta"
        End Select
    End Function
End Class