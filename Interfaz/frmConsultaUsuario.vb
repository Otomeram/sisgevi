﻿Public Class frmConsultaUsuario
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmUsuarios.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        sql = "SELECT * FROM Usuario INNER JOIN TelefonoUsuario ON Usuario.ci = TelefonoUsuario.ci"
        fullFields = txtApellido.Text.Any Or txtCalle.Text.Any Or txtCI.Text.Any Or txtTelefono.Text.Any Or txtEsquina.Text.Any Or txtNombre.Text.Any Or txtNPuerta.Text.Any Or chkRol.Checked
        If fullFields Then
            sql += " WHERE"
        End If
        If txtApellido.Text.Any Then
            sql += " Usuario.apellido = '" & txtApellido.Text & "'"
            If txtCalle.Text.Any Or txtCI.Text.Any Or txtEsquina.Text.Any Or txtNombre.Text.Any Or txtTelefono.Text.Any Or txtNPuerta.Text.Any Or chkRol.Checked Then
                sql += " AND"
            End If
        End If
        If txtCalle.Text.Any Then
            sql += " Usuario.calle = '" & txtCalle.Text & "'"
            If txtCI.Text.Any Or txtEsquina.Text.Any Or txtNombre.Text.Any Or txtTelefono.Text.Any Or txtNPuerta.Text.Any Or chkRol.Checked Then
                sql += " AND"
            End If
        End If
        If txtCI.Text.Any And IsNumeric(txtCI.Text) Then
            sql += " Usuario.ci = '" & txtCI.Text & "'"
            If txtEsquina.Text.Any Or txtNombre.Text.Any Or txtTelefono.Text.Any Or txtNPuerta.Text.Any Or chkRol.Checked Then
                sql += " AND"
            End If
        ElseIf Not IsNumeric(txtCI.Text) And txtCI.Text.Any Then
            MsgBox("La cédula ingresada contiene caracteres inválidos")
            Exit Sub
        End If
        If txtEsquina.Text.Any Then
            sql += " Usuario.esquina = '" & txtEsquina.Text & "'"
            If txtNombre.Text.Any Or txtTelefono.Text.Any Or txtNPuerta.Text.Any Or chkRol.Checked Then
                sql += " AND"
            End If
        End If
        If txtNombre.Text.Any Then
            sql += " Usuario.nombre = '" & txtNombre.Text & "'"
            If txtNPuerta.Text.Any Or txtTelefono.Text.Any Or chkRol.Checked Then
                sql += " AND"
            End If
        End If
        If txtNPuerta.Text.Any And IsNumeric(txtNPuerta.Text) Then
            sql += " Usuario.n_puerta = '" & txtNPuerta.Text & "'"
            If chkRol.Checked Or txtTelefono.Text.Any Then
                sql += " AND"
            End If
        ElseIf Not IsNumeric(txtNPuerta.Text) And txtNPuerta.Text.Any Then
            MsgBox("El número de puerta ingresado contiene caracteres inválidos")
            Exit Sub
        End If
        If txtTelefono.Text.Any Then
            sql += " TelefonoUsuario.telefono = '" & txtTelefono.Text & "'"
            If chkRol.Checked Then
                sql += " AND"
            End If
        End If
        If chkRol.Checked Then
            sql += " Usuario.rol = '" & cbRol.Text & "'"
        End If
        display(dgvResultado)
    End Sub

    Private Sub frmConsultaUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbRol)
    End Sub
End Class