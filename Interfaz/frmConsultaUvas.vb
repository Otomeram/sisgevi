﻿
Public Class frmConsultaDeUvas
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        Select Case rol
            Case "Administrativo"
                frmConsultasAdministrativo.Show()
            Case "Asesor"
                frmConsultasAsesor.Show()
        End Select
        Dispose()
    End Sub

    Private Sub frmConsultaDeUvas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbUvas, "nombre", "Producto")
    End Sub
End Class