﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultasAdministrativo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnTransporte = New System.Windows.Forms.Button()
        Me.btnUvas = New System.Windows.Forms.Button()
        Me.btnProcesos = New System.Windows.Forms.Button()
        Me.btnRecipientes = New System.Windows.Forms.Button()
        Me.btnReservas = New System.Windows.Forms.Button()
        Me.btnProductos = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnTransporte
        '
        Me.btnTransporte.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnTransporte.Location = New System.Drawing.Point(54, 35)
        Me.btnTransporte.Name = "btnTransporte"
        Me.btnTransporte.Size = New System.Drawing.Size(231, 66)
        Me.btnTransporte.TabIndex = 1
        Me.btnTransporte.Text = "Transporte"
        Me.btnTransporte.UseVisualStyleBackColor = True
        '
        'btnUvas
        '
        Me.btnUvas.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUvas.Location = New System.Drawing.Point(337, 35)
        Me.btnUvas.Name = "btnUvas"
        Me.btnUvas.Size = New System.Drawing.Size(231, 66)
        Me.btnUvas.TabIndex = 2
        Me.btnUvas.Text = "Uvas"
        Me.btnUvas.UseVisualStyleBackColor = True
        '
        'btnProcesos
        '
        Me.btnProcesos.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcesos.Location = New System.Drawing.Point(54, 147)
        Me.btnProcesos.Name = "btnProcesos"
        Me.btnProcesos.Size = New System.Drawing.Size(231, 66)
        Me.btnProcesos.TabIndex = 3
        Me.btnProcesos.Text = "Procesos"
        Me.btnProcesos.UseVisualStyleBackColor = True
        '
        'btnRecipientes
        '
        Me.btnRecipientes.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecipientes.Location = New System.Drawing.Point(337, 147)
        Me.btnRecipientes.Name = "btnRecipientes"
        Me.btnRecipientes.Size = New System.Drawing.Size(231, 66)
        Me.btnRecipientes.TabIndex = 4
        Me.btnRecipientes.Text = "Recipientes"
        Me.btnRecipientes.UseVisualStyleBackColor = True
        '
        'btnReservas
        '
        Me.btnReservas.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReservas.Location = New System.Drawing.Point(54, 262)
        Me.btnReservas.Name = "btnReservas"
        Me.btnReservas.Size = New System.Drawing.Size(231, 66)
        Me.btnReservas.TabIndex = 5
        Me.btnReservas.Text = "Reservas"
        Me.btnReservas.UseVisualStyleBackColor = True
        '
        'btnProductos
        '
        Me.btnProductos.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProductos.Location = New System.Drawing.Point(337, 262)
        Me.btnProductos.Name = "btnProductos"
        Me.btnProductos.Size = New System.Drawing.Size(231, 66)
        Me.btnProductos.TabIndex = 6
        Me.btnProductos.Text = "Productos"
        Me.btnProductos.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(23, 335)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'frmConsultasAdministrativo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnProductos)
        Me.Controls.Add(Me.btnReservas)
        Me.Controls.Add(Me.btnRecipientes)
        Me.Controls.Add(Me.btnProcesos)
        Me.Controls.Add(Me.btnUvas)
        Me.Controls.Add(Me.btnTransporte)
        Me.Name = "frmConsultasAdministrativo"
        Me.Text = "Consultas administrativo"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnTransporte As System.Windows.Forms.Button
    Friend WithEvents btnUvas As System.Windows.Forms.Button
    Friend WithEvents btnProcesos As System.Windows.Forms.Button
    Friend WithEvents btnRecipientes As System.Windows.Forms.Button
    Friend WithEvents btnReservas As System.Windows.Forms.Button
    Friend WithEvents btnProductos As Button
    Friend WithEvents btnVolver As Button
End Class
