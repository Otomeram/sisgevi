﻿Public Class frmConsultasAdministrativo
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnTransporte_Click(sender As Object, e As EventArgs) Handles btnTransporte.Click
        frmConsultaTransporte.Show()
        Dispose()
    End Sub

    Private Sub btnUvas_Click(sender As Object, e As EventArgs) Handles btnUvas.Click
        frmConsultaDeUvas.Show()
        Dispose()
    End Sub

    Private Sub btnProcesos_Click(sender As Object, e As EventArgs) Handles btnProcesos.Click
        frmConsultaProceso.Show()
        Dispose()
    End Sub

    Private Sub btnRecipientes_Click(sender As Object, e As EventArgs) Handles btnRecipientes.Click
        frmConsultaRecipiente.Show()
        Dispose()
    End Sub

    Private Sub btnReservas_Click(sender As Object, e As EventArgs) Handles btnReservas.Click
        frmConsultaReserva.Show()
        Dispose()
    End Sub

    Private Sub btnProductos_Click(sender As Object, e As EventArgs) Handles btnProductos.Click
        frmConsultaProducto.Show()
        Dispose()
    End Sub

    Private Sub frmConsultasAdministrativo_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class