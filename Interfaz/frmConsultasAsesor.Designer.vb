﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmConsultasAsesor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnUvas = New System.Windows.Forms.Button()
        Me.btnProcesos = New System.Windows.Forms.Button()
        Me.btnRecipientes = New System.Windows.Forms.Button()
        Me.btnProductos = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnUvas
        '
        Me.btnUvas.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnUvas.Location = New System.Drawing.Point(47, 106)
        Me.btnUvas.Name = "btnUvas"
        Me.btnUvas.Size = New System.Drawing.Size(231, 66)
        Me.btnUvas.TabIndex = 1
        Me.btnUvas.Text = "Uvas"
        Me.btnUvas.UseVisualStyleBackColor = True
        '
        'btnProcesos
        '
        Me.btnProcesos.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcesos.Location = New System.Drawing.Point(322, 106)
        Me.btnProcesos.Name = "btnProcesos"
        Me.btnProcesos.Size = New System.Drawing.Size(231, 66)
        Me.btnProcesos.TabIndex = 2
        Me.btnProcesos.Text = "Procesos"
        Me.btnProcesos.UseVisualStyleBackColor = True
        '
        'btnRecipientes
        '
        Me.btnRecipientes.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecipientes.Location = New System.Drawing.Point(47, 211)
        Me.btnRecipientes.Name = "btnRecipientes"
        Me.btnRecipientes.Size = New System.Drawing.Size(231, 66)
        Me.btnRecipientes.TabIndex = 3
        Me.btnRecipientes.Text = "Recipientes"
        Me.btnRecipientes.UseVisualStyleBackColor = True
        '
        'btnProductos
        '
        Me.btnProductos.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProductos.Location = New System.Drawing.Point(322, 211)
        Me.btnProductos.Name = "btnProductos"
        Me.btnProductos.Size = New System.Drawing.Size(231, 66)
        Me.btnProductos.TabIndex = 4
        Me.btnProductos.Text = "Productos"
        Me.btnProductos.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(30, 323)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'frmConsultasAsesor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnProductos)
        Me.Controls.Add(Me.btnRecipientes)
        Me.Controls.Add(Me.btnProcesos)
        Me.Controls.Add(Me.btnUvas)
        Me.Name = "frmConsultasAsesor"
        Me.Text = "Consultas asesor"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnUvas As System.Windows.Forms.Button
    Friend WithEvents btnProcesos As System.Windows.Forms.Button
    Friend WithEvents btnRecipientes As System.Windows.Forms.Button
    Friend WithEvents btnProductos As Button
    Friend WithEvents btnVolver As Button
End Class
