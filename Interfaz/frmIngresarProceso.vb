﻿Public Class frmIngresarProceso
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmIngresos.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        sql = "INSERT INTO Pasa Values('" & cbProceso.Text & "', '" & cbIdRecipiente.Text & "', '" & dtpFechaInicio.Value.ToString("dd/MM/yyyy") & "', '" & dtpFechaFin.Value.ToString("dd/MM/yyyy") & "')"
        cn.Execute(sql)
        MsgBox("El proceso fue ingresado exitosamente")
    End Sub

    Private Sub frmIngresarProceso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbProceso, "nombre", "Proceso")
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class