﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngresarProducto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbUvas = New System.Windows.Forms.ComboBox()
        Me.lblTipoUva = New System.Windows.Forms.Label()
        Me.txtLote = New System.Windows.Forms.TextBox()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.lblLote = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.dtpFechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaInicio = New System.Windows.Forms.Label()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaFin = New System.Windows.Forms.Label()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.cbTipoDeRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblTipoDeRecipiente = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cbUvas
        '
        Me.cbUvas.FormattingEnabled = True
        Me.cbUvas.Location = New System.Drawing.Point(287, 98)
        Me.cbUvas.Name = "cbUvas"
        Me.cbUvas.Size = New System.Drawing.Size(121, 21)
        Me.cbUvas.TabIndex = 26
        '
        'lblTipoUva
        '
        Me.lblTipoUva.AutoSize = True
        Me.lblTipoUva.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoUva.Location = New System.Drawing.Point(148, 97)
        Me.lblTipoUva.Name = "lblTipoUva"
        Me.lblTipoUva.Size = New System.Drawing.Size(80, 19)
        Me.lblTipoUva.TabIndex = 25
        Me.lblTipoUva.Text = "Tipo de uva"
        '
        'txtLote
        '
        Me.txtLote.Location = New System.Drawing.Point(288, 132)
        Me.txtLote.Name = "txtLote"
        Me.txtLote.Size = New System.Drawing.Size(120, 20)
        Me.txtLote.TabIndex = 24
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(288, 63)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(70, 21)
        Me.cbIdRecipiente.TabIndex = 23
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(148, 62)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(84, 19)
        Me.lblIdRecipiente.TabIndex = 21
        Me.lblIdRecipiente.Text = "Id recipiente"
        '
        'lblLote
        '
        Me.lblLote.AutoSize = True
        Me.lblLote.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLote.Location = New System.Drawing.Point(148, 131)
        Me.lblLote.Name = "lblLote"
        Me.lblLote.Size = New System.Drawing.Size(37, 19)
        Me.lblLote.TabIndex = 19
        Me.lblLote.Text = "Lote"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(288, 165)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(120, 20)
        Me.txtCantidad.TabIndex = 28
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantidad.Location = New System.Drawing.Point(148, 164)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(64, 19)
        Me.lblCantidad.TabIndex = 27
        Me.lblCantidad.Text = "Cantidad"
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Location = New System.Drawing.Point(288, 200)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaInicio.TabIndex = 30
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.AutoSize = True
        Me.lblFechaInicio.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaInicio.Location = New System.Drawing.Point(148, 200)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(100, 19)
        Me.lblFechaInicio.TabIndex = 29
        Me.lblFechaInicio.Text = "Fecha de inicio"
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Location = New System.Drawing.Point(288, 237)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaFin.TabIndex = 32
        '
        'lblFechaFin
        '
        Me.lblFechaFin.AutoSize = True
        Me.lblFechaFin.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFin.Location = New System.Drawing.Point(148, 237)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(83, 19)
        Me.lblFechaFin.TabIndex = 31
        Me.lblFechaFin.Text = "Fecha de fin"
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(99, 281)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 34
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(449, 281)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 33
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'cbTipoDeRecipiente
        '
        Me.cbTipoDeRecipiente.FormattingEnabled = True
        Me.cbTipoDeRecipiente.Location = New System.Drawing.Point(288, 33)
        Me.cbTipoDeRecipiente.Name = "cbTipoDeRecipiente"
        Me.cbTipoDeRecipiente.Size = New System.Drawing.Size(120, 21)
        Me.cbTipoDeRecipiente.TabIndex = 36
        '
        'lblTipoDeRecipiente
        '
        Me.lblTipoDeRecipiente.AutoSize = True
        Me.lblTipoDeRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente.Location = New System.Drawing.Point(148, 33)
        Me.lblTipoDeRecipiente.Name = "lblTipoDeRecipiente"
        Me.lblTipoDeRecipiente.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente.TabIndex = 35
        Me.lblTipoDeRecipiente.Text = "Tipo de recipiente"
        '
        'frmIngresarProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.cbTipoDeRecipiente)
        Me.Controls.Add(Me.lblTipoDeRecipiente)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.dtpFechaFin)
        Me.Controls.Add(Me.lblFechaFin)
        Me.Controls.Add(Me.dtpFechaInicio)
        Me.Controls.Add(Me.lblFechaInicio)
        Me.Controls.Add(Me.txtCantidad)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.cbUvas)
        Me.Controls.Add(Me.lblTipoUva)
        Me.Controls.Add(Me.txtLote)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.lblLote)
        Me.Name = "frmIngresarProducto"
        Me.Text = "Ingresar Producto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbUvas As ComboBox
    Friend WithEvents lblTipoUva As Label
    Friend WithEvents txtLote As TextBox
    Friend WithEvents cbIdRecipiente As ComboBox
    Friend WithEvents lblIdRecipiente As Label
    Friend WithEvents lblLote As Label
    Friend WithEvents txtCantidad As TextBox
    Friend WithEvents lblCantidad As Label
    Friend WithEvents dtpFechaInicio As DateTimePicker
    Friend WithEvents lblFechaInicio As Label
    Friend WithEvents dtpFechaFin As DateTimePicker
    Friend WithEvents lblFechaFin As Label
    Friend WithEvents btnVolver As Button
    Friend WithEvents btnIngresar As Button
    Friend WithEvents cbTipoDeRecipiente As ComboBox
    Friend WithEvents lblTipoDeRecipiente As Label
End Class
