﻿Public Class frmIngresarProducto
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmIngresos.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        validInput = IsNumeric(txtCantidad.Text) And IsNumeric(txtLote.Text)
        fullFields = txtLote.Text.Any And txtCantidad.Text.Any
        If validInput And fullFields Then
            sql = "INSERT INTO Va Values('" & cbIdRecipiente.Text & "', '" & cbUvas.Text & "', '" & txtLote.Text & "', '" & txtCantidad.Text & "''" & dtpFechaInicio.Value.ToString("dd/MM/yyyy") & "''" & dtpFechaFin.Value.ToString("dd/MM/yyyy") & "' )"
            cn.Execute(sql)
            MsgBox("El producto fue ingresado exitosamente")
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("Uno o más campos numéricos contienen caracteres inválidos")
        End If

    End Sub

    Private Sub frmIngresarProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbUvas, "nombre", "Producto")
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class