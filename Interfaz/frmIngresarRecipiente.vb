﻿Public Class frmIngresarRecipiente
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmIngresos.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If IsNumeric(txtCapacidad.Text) Then
            sql = "INSERT INTO Recipiente (tipo,capacidad) VALUES ('" & cbTipo.Text & "' ,'" & txtCapacidad.Text & "')"
            cn.Execute(sql)
            MsgBox("El recipiente fue ingresado exitosamente")
        Else
            MsgBox("La cantidad ingresada contiene caracteres inválidos")
        End If
    End Sub

    Private Sub btnAddTel_Click(sender As Object, e As EventArgs) Handles btnAddTel.Click
        addItem(cbTipo, txtTipo)
    End Sub

    Private Sub frmIngresarRecipiente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbTipo, "tipo", "Recipiente")
    End Sub
End Class