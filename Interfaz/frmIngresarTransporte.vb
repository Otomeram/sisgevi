﻿Public Class frmIngresarTransporte
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmIngresos.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = txtDestino.Text.Any And txtHoraFin.Text.Any And txtHoraInicio.Text.Any And txtMinutoFin.Text.Any And txtMinutoInicio.Text.Any And txtOrigen.Text.Any
        validInput = IsNumeric(txtHoraFin.Text) And IsNumeric(txtHoraInicio.Text) And IsNumeric(txtMinutoFin.Text) And IsNumeric(txtMinutoInicio.Text)
        If fullFields And validInput Then
            sql = "INSERT INTO Transporta Values('" & ci & "', '" & cbIdRecipiente.Text & "','" & txtOrigen.Text & "', '" & txtDestino.Text & "', '" & txtHoraInicio.Text & ":" & txtMinutoInicio.Text & "', '" & txtHoraFin.Text & ":" & txtMinutoFin.Text & "', '" & dtpFecha.Value.ToString("dd/MM/yyyy") & "')"
            cn.Execute(sql)
            MsgBox("El transporte fue ingresado exitosamente")
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("Uno o más campos numéricos contienen caracteres inválidos")
        End If
    End Sub

    Private Sub frmIngresarTransporte_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class