﻿Public Class frmIngresarTrasiego
    Dim stock As Integer
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmIngresos.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = txtCantidad.Text.Any And txtCapacidad.Text.Any And txtTipoVino.Text.Any
        validInput = IsNumeric(txtCantidad.Text) And IsNumeric(txtCapacidad.Text)
        If fullFields And validInput Then
            stock = Math.Floor(Val(txtCantidad.Text) / Val(txtCapacidad.Text))
            sql = "INSERT INTO Trasiega (recipiente,fecha,cantidad) Values('" & cbIdRecipiente.SelectedItem.ToString() & "', '" & dtpFecha.Value.ToString("dd/MM/yyyy") & "', '" & txtCantidad.Text & "')"
            cn.Execute(sql)
            sql = "INSERT INTO Botella (stock,capacidad,tipo_vino) Values('" & stock & "','" & txtCapacidad.Text & "', '" & txtTipoVino.Text & "')"
            cn.Execute(sql)
            MsgBox("El trasiego fue ingresado exitosamente")
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("Uno o más campos numéricos contienen caracteres inválidos")
        End If
    End Sub

    Private Sub frmIngresarTrasiego_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class