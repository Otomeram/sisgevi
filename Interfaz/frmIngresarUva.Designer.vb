﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngresarUva
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtTipoUva = New System.Windows.Forms.TextBox()
        Me.lblTipoUva = New System.Windows.Forms.Label()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtTipoUva
        '
        Me.txtTipoUva.Location = New System.Drawing.Point(260, 134)
        Me.txtTipoUva.Name = "txtTipoUva"
        Me.txtTipoUva.Size = New System.Drawing.Size(180, 20)
        Me.txtTipoUva.TabIndex = 24
        '
        'lblTipoUva
        '
        Me.lblTipoUva.AutoSize = True
        Me.lblTipoUva.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoUva.Location = New System.Drawing.Point(151, 133)
        Me.lblTipoUva.Name = "lblTipoUva"
        Me.lblTipoUva.Size = New System.Drawing.Size(80, 19)
        Me.lblTipoUva.TabIndex = 23
        Me.lblTipoUva.Text = "Tipo de uva"
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(98, 237)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 26
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(448, 237)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 25
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'frmIngresarUva
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.txtTipoUva)
        Me.Controls.Add(Me.lblTipoUva)
        Me.Name = "frmIngresarUva"
        Me.Text = "Ingresar Uva"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtTipoUva As TextBox
    Friend WithEvents lblTipoUva As Label
    Friend WithEvents btnVolver As Button
    Friend WithEvents btnIngresar As Button
End Class
