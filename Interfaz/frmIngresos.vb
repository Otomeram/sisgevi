﻿Public Class frmIngresos
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmMenuAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnProducto_Click(sender As Object, e As EventArgs) Handles btnProducto.Click
        frmIngresarProducto.Show()
        Dispose()
    End Sub

    Private Sub btnUva_Click(sender As Object, e As EventArgs) Handles btnUva.Click
        frmIngresarUva.Show()
        Dispose()
    End Sub

    Private Sub btnRecipiente_Click(sender As Object, e As EventArgs) Handles btnRecipiente.Click
        frmIngresarRecipiente.Show()
        Dispose()
    End Sub

    Private Sub btnProceso_Click(sender As Object, e As EventArgs) Handles btnProceso.Click
        frmIngresarProceso.Show()
        Dispose()
    End Sub

    Private Sub btnTrasiego_Click(sender As Object, e As EventArgs) Handles btnTrasiego.Click
        frmIngresarTrasiego.Show()
        Dispose()
    End Sub

    Private Sub btnProceso1_Click_1(sender As Object, e As EventArgs) Handles btnProceso1.Click
        frmAltaProceso.Show()
        Dispose()
    End Sub

End Class