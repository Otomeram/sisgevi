﻿Public Class frmLogin
    Dim login As String
    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = txtCi.Text.Any And txtContraseña.Text.Any
        validInput = IsNumeric(txtCi.Text)
        If fullFields And validInput Then
            ci = txtCi.Text
            pass = txtContraseña.Text
            login = "u" & ci
            If cn.State = 1 Then
                cn.Close()
            End If
            Try
                cn.Open("miodbc", login, pass)
            Catch ex As Exception
                MsgBox("Usuario y/o contraseña incorrecto(s)")
                Exit Sub
            End Try
            sql = "SELECT Rol FROM Usuario WHERE CI = '" & ci & "'"
            rs.Open(sql, cn)
            rol = rs(0).Value
            rs.Close()
            Redirect()
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("La CI ingresada contiene caracteres inválidos")
        End If

    End Sub
    Private Sub Redirect()
        Select Case rol
            Case "Gerente"
                frmMenuGerente.Show()
            Case "Administrativo"
                frmMenuAdministrativo.Show()
            Case "Cliente"
                frmMenuCliente.Show()
            Case "Asesor"
                frmMenuAsesor.Show()
            Case "Conductor"
                frmMenuConductor.Show()
        End Select
        Hide()
    End Sub
End Class