﻿
Public Class frmMenuAdministrativo
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmLogin.Show()
        Dispose()
    End Sub

    Private Sub btnConsultas_Click(sender As Object, e As EventArgs) Handles btnConsultas.Click
        frmConsultasAdministrativo.Show()
        Dispose()
    End Sub

    Private Sub btnModificaciones_Click(sender As Object, e As EventArgs) Handles btnModificaciones.Click
        frmModificaciones.Show()
        Dispose()
    End Sub

    Private Sub btnIngresos_Click(sender As Object, e As EventArgs) Handles btnIngresos.Click
        frmIngresos.Show()
        Dispose()
    End Sub

    Private Sub btnBorrados_Click(sender As Object, e As EventArgs) Handles btnBorrados.Click
        frmBorradosAdministrativo.Show()
        Dispose()
    End Sub
End Class