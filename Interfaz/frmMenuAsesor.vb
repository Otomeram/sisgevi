﻿Public Class frmMenuAsesor
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmLogin.Show()
        Dispose()
    End Sub

    Private Sub btnConsultas_Click(sender As Object, e As EventArgs) Handles btnConsultas.Click
        frmConsultasAsesor.Show()
        Dispose()
    End Sub

    Private Sub btnObservaciones_Click(sender As Object, e As EventArgs) Handles btnObservaciones.Click
        frmObservacion.Show()
        Dispose()
    End Sub

End Class