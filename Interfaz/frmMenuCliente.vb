﻿Public Class frmMenuCliente
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmLogin.Show()
        Dispose()
    End Sub

    Private Sub btnReservas_Click(sender As Object, e As EventArgs) Handles btnReservas.Click
        frmReservas.Show()
        Dispose()
    End Sub

    Private Sub btnConsultas_Click(sender As Object, e As EventArgs) Handles btnConsultas.Click
        frmConsultaProducto.Show()
        Dispose()
    End Sub
End Class