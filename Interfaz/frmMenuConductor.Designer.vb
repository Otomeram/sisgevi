﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenuConductor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnIngresarTransporte = New System.Windows.Forms.Button()
        Me.btnAgendarTransporte = New System.Windows.Forms.Button()
        Me.btnConsultaProducto = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnIngresarTransporte
        '
        Me.btnIngresarTransporte.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIngresarTransporte.Location = New System.Drawing.Point(338, 73)
        Me.btnIngresarTransporte.Name = "btnIngresarTransporte"
        Me.btnIngresarTransporte.Size = New System.Drawing.Size(231, 66)
        Me.btnIngresarTransporte.TabIndex = 3
        Me.btnIngresarTransporte.Text = "Ingresar transporte"
        Me.btnIngresarTransporte.UseVisualStyleBackColor = True
        '
        'btnAgendarTransporte
        '
        Me.btnAgendarTransporte.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAgendarTransporte.Location = New System.Drawing.Point(44, 73)
        Me.btnAgendarTransporte.Name = "btnAgendarTransporte"
        Me.btnAgendarTransporte.Size = New System.Drawing.Size(231, 66)
        Me.btnAgendarTransporte.TabIndex = 4
        Me.btnAgendarTransporte.Text = "Agendar transporte"
        Me.btnAgendarTransporte.UseVisualStyleBackColor = True
        '
        'btnConsultaProducto
        '
        Me.btnConsultaProducto.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConsultaProducto.Location = New System.Drawing.Point(195, 200)
        Me.btnConsultaProducto.Name = "btnConsultaProducto"
        Me.btnConsultaProducto.Size = New System.Drawing.Size(231, 66)
        Me.btnConsultaProducto.TabIndex = 5
        Me.btnConsultaProducto.Text = "Consultas de producto"
        Me.btnConsultaProducto.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(31, 320)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'frmMenuConductor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnConsultaProducto)
        Me.Controls.Add(Me.btnAgendarTransporte)
        Me.Controls.Add(Me.btnIngresarTransporte)
        Me.Name = "frmMenuConductor"
        Me.Text = "Menú conductor"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnIngresarTransporte As Button
    Friend WithEvents btnAgendarTransporte As Button
    Friend WithEvents btnConsultaProducto As Button
    Friend WithEvents btnVolver As Button
End Class
