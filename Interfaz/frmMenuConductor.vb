﻿Public Class frmMenuConductor
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmLogin.Show()
        Dispose()
    End Sub

    Private Sub btnConsultaProducto_Click(sender As Object, e As EventArgs) Handles btnConsultaProducto.Click
        frmConsultaProducto.Show()
        Dispose()
    End Sub

    Private Sub btnAgendarTransporte_Click(sender As Object, e As EventArgs) Handles btnAgendarTransporte.Click
        frmAgendarTransporte.Show()
        Dispose()
    End Sub

    Private Sub btnIngresarTransporte_Click(sender As Object, e As EventArgs) Handles btnIngresarTransporte.Click
        frmIngresarTransporte.Show()
        Dispose()
    End Sub
End Class