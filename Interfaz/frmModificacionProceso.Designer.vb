﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificacionProceso
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaFin = New System.Windows.Forms.Label()
        Me.dtpFechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaInicio = New System.Windows.Forms.Label()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.cbProceso = New System.Windows.Forms.ComboBox()
        Me.lblProceso = New System.Windows.Forms.Label()
        Me.dtpfechaInicio1 = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaDeInicio1 = New System.Windows.Forms.Label()
        Me.cbIdRecipiente1 = New System.Windows.Forms.ComboBox()
        Me.lblIdRecipiente1 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.cbTipoDeRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblTipoDeRecipiente = New System.Windows.Forms.Label()
        Me.cbTipoDeRecipiente1 = New System.Windows.Forms.ComboBox()
        Me.lblTipoDeRecipiente1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(89, 314)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 56
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(453, 314)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 55
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Location = New System.Drawing.Point(294, 287)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaFin.TabIndex = 54
        '
        'lblFechaFin
        '
        Me.lblFechaFin.AutoSize = True
        Me.lblFechaFin.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFin.Location = New System.Drawing.Point(105, 289)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(83, 19)
        Me.lblFechaFin.TabIndex = 53
        Me.lblFechaFin.Text = "Fecha de fin"
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Location = New System.Drawing.Point(294, 257)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaInicio.TabIndex = 52
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.AutoSize = True
        Me.lblFechaInicio.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaInicio.Location = New System.Drawing.Point(105, 259)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(100, 19)
        Me.lblFechaInicio.TabIndex = 51
        Me.lblFechaInicio.Text = "Fecha de inicio"
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(294, 226)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(151, 21)
        Me.cbIdRecipiente.TabIndex = 50
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(105, 225)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(106, 19)
        Me.lblIdRecipiente.TabIndex = 49
        Me.lblIdRecipiente.Text = "Id del recipiente"
        '
        'cbProceso
        '
        Me.cbProceso.FormattingEnabled = True
        Me.cbProceso.Location = New System.Drawing.Point(294, 167)
        Me.cbProceso.Name = "cbProceso"
        Me.cbProceso.Size = New System.Drawing.Size(151, 21)
        Me.cbProceso.TabIndex = 48
        '
        'lblProceso
        '
        Me.lblProceso.AutoSize = True
        Me.lblProceso.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProceso.Location = New System.Drawing.Point(105, 170)
        Me.lblProceso.Name = "lblProceso"
        Me.lblProceso.Size = New System.Drawing.Size(59, 19)
        Me.lblProceso.TabIndex = 47
        Me.lblProceso.Text = "Proceso"
        '
        'dtpfechaInicio1
        '
        Me.dtpfechaInicio1.Location = New System.Drawing.Point(294, 90)
        Me.dtpfechaInicio1.Name = "dtpfechaInicio1"
        Me.dtpfechaInicio1.Size = New System.Drawing.Size(200, 20)
        Me.dtpfechaInicio1.TabIndex = 62
        '
        'lblFechaDeInicio1
        '
        Me.lblFechaDeInicio1.AutoSize = True
        Me.lblFechaDeInicio1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaDeInicio1.Location = New System.Drawing.Point(105, 92)
        Me.lblFechaDeInicio1.Name = "lblFechaDeInicio1"
        Me.lblFechaDeInicio1.Size = New System.Drawing.Size(100, 19)
        Me.lblFechaDeInicio1.TabIndex = 61
        Me.lblFechaDeInicio1.Text = "Fecha de inicio"
        '
        'cbIdRecipiente1
        '
        Me.cbIdRecipiente1.FormattingEnabled = True
        Me.cbIdRecipiente1.Location = New System.Drawing.Point(294, 57)
        Me.cbIdRecipiente1.Name = "cbIdRecipiente1"
        Me.cbIdRecipiente1.Size = New System.Drawing.Size(151, 21)
        Me.cbIdRecipiente1.TabIndex = 60
        '
        'lblIdRecipiente1
        '
        Me.lblIdRecipiente1.AutoSize = True
        Me.lblIdRecipiente1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente1.Location = New System.Drawing.Point(105, 56)
        Me.lblIdRecipiente1.Name = "lblIdRecipiente1"
        Me.lblIdRecipiente1.Size = New System.Drawing.Size(106, 19)
        Me.lblIdRecipiente1.TabIndex = 59
        Me.lblIdRecipiente1.Text = "Id del recipiente"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(510, 87)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 63
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'cbTipoDeRecipiente
        '
        Me.cbTipoDeRecipiente.FormattingEnabled = True
        Me.cbTipoDeRecipiente.Location = New System.Drawing.Point(294, 199)
        Me.cbTipoDeRecipiente.Name = "cbTipoDeRecipiente"
        Me.cbTipoDeRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbTipoDeRecipiente.TabIndex = 65
        '
        'lblTipoDeRecipiente
        '
        Me.lblTipoDeRecipiente.AutoSize = True
        Me.lblTipoDeRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente.Location = New System.Drawing.Point(105, 198)
        Me.lblTipoDeRecipiente.Name = "lblTipoDeRecipiente"
        Me.lblTipoDeRecipiente.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente.TabIndex = 64
        Me.lblTipoDeRecipiente.Text = "Tipo de recipiente"
        '
        'cbTipoDeRecipiente1
        '
        Me.cbTipoDeRecipiente1.FormattingEnabled = True
        Me.cbTipoDeRecipiente1.Location = New System.Drawing.Point(294, 29)
        Me.cbTipoDeRecipiente1.Name = "cbTipoDeRecipiente1"
        Me.cbTipoDeRecipiente1.Size = New System.Drawing.Size(121, 21)
        Me.cbTipoDeRecipiente1.TabIndex = 67
        '
        'lblTipoDeRecipiente1
        '
        Me.lblTipoDeRecipiente1.AutoSize = True
        Me.lblTipoDeRecipiente1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente1.Location = New System.Drawing.Point(105, 31)
        Me.lblTipoDeRecipiente1.Name = "lblTipoDeRecipiente1"
        Me.lblTipoDeRecipiente1.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente1.TabIndex = 66
        Me.lblTipoDeRecipiente1.Text = "Tipo de recipiente"
        '
        'frmModificacionProceso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.cbTipoDeRecipiente1)
        Me.Controls.Add(Me.lblTipoDeRecipiente1)
        Me.Controls.Add(Me.cbTipoDeRecipiente)
        Me.Controls.Add(Me.lblTipoDeRecipiente)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.dtpfechaInicio1)
        Me.Controls.Add(Me.lblFechaDeInicio1)
        Me.Controls.Add(Me.cbIdRecipiente1)
        Me.Controls.Add(Me.lblIdRecipiente1)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.dtpFechaFin)
        Me.Controls.Add(Me.lblFechaFin)
        Me.Controls.Add(Me.dtpFechaInicio)
        Me.Controls.Add(Me.lblFechaInicio)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.cbProceso)
        Me.Controls.Add(Me.lblProceso)
        Me.Name = "frmModificacionProceso"
        Me.Text = "Modificaciones de procesos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnVolver As Button
    Friend WithEvents btnIngresar As Button
    Friend WithEvents dtpFechaFin As DateTimePicker
    Friend WithEvents lblFechaFin As Label
    Friend WithEvents dtpFechaInicio As DateTimePicker
    Friend WithEvents lblFechaInicio As Label
    Friend WithEvents cbIdRecipiente As ComboBox
    Friend WithEvents lblIdRecipiente As Label
    Friend WithEvents cbProceso As ComboBox
    Friend WithEvents lblProceso As Label
    Friend WithEvents dtpfechaInicio1 As DateTimePicker
    Friend WithEvents lblFechaDeInicio1 As Label
    Friend WithEvents cbIdRecipiente1 As ComboBox
    Friend WithEvents lblIdRecipiente1 As Label
    Friend WithEvents btnBuscar As Button
    Friend WithEvents cbTipoDeRecipiente As ComboBox
    Friend WithEvents lblTipoDeRecipiente As Label
    Friend WithEvents cbTipoDeRecipiente1 As ComboBox
    Friend WithEvents lblTipoDeRecipiente1 As Label
End Class
