﻿Public Class frmModificacionProceso
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmModificaciones.Show()
        Dispose()
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        sql = "SELECT * FROM Pasa WHERE id_recipiente = '" & cbIdRecipiente1.SelectedItem.ToString() & "' AND fecha = '" & dtpfechaInicio1.Value.ToString("dd/MM/yyyy") & "'"
        rs.Open(sql, cn)
        If rs.RecordCount = 0 Then
            MsgBox("No se han encontrado resultados coincidentes con los parámetros de búsqueda ingresados")
        Else
            cbProceso.SelectedItem = rs("nombre").Value
            cbTipoDeRecipiente.SelectedItem = rs("tipo").Value
            cbIdRecipiente.SelectedItem = rs("id_recipiente").Value
            dtpFechaInicio.Value = rs("fecha_inicio").Value
            dtpFechaFin.Value = rs("fecha_fin").Value
            btnIngresar.Enabled = True
        End If
        If rs.State = 1 Then
            rs.Close()
        End If
    End Sub

    Private Sub frmModificacionProceso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbProceso, "nombre", "Proceso")
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
        fill(cbTipoDeRecipiente1, "tipo", "Recipiente")
        btnIngresar.Enabled = False
    End Sub

    Private Sub cbTipoDeRecipiente1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente1.SelectedIndexChanged
        updateId(cbIdRecipiente1, cbTipoDeRecipiente1.SelectedItem.ToString())
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.SelectedItem.ToString())
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        sql = "UPDATE Pasa SET id_recipiente = '" & cbIdRecipiente.SelectedItem.ToString() & "', nombre = '" & cbProceso.SelectedItem.ToString() & "', fecha_inicio = '" & dtpFechaInicio.Value.ToString("dd/MM/yyyy") & "', fecha_fin = '" & dtpFechaFin.Value.ToString("dd/MM/yyyy") & "' WHERE id_recipiente = '" & cbIdRecipiente1.SelectedItem.ToString() & "' AND fecha = '" & dtpfechaInicio1.Value.ToString("dd/MM/yyyy") & "'"
        cn.Execute(sql)
        MsgBox("La modificación fue realizada exitosamente")
    End Sub
End Class