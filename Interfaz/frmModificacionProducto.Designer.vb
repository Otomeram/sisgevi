﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmModificacionProducto
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.dtpFechaFin = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaFin = New System.Windows.Forms.Label()
        Me.dtpFechaInicio = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaInicio = New System.Windows.Forms.Label()
        Me.txtCantidad = New System.Windows.Forms.TextBox()
        Me.lblCantidad = New System.Windows.Forms.Label()
        Me.cbUvas = New System.Windows.Forms.ComboBox()
        Me.lblUvas = New System.Windows.Forms.Label()
        Me.txtLote = New System.Windows.Forms.TextBox()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.lblLote = New System.Windows.Forms.Label()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.cbIdRecipiente1 = New System.Windows.Forms.ComboBox()
        Me.lblIngresar1 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.cbTipoDeRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblTipoDeRecipiente = New System.Windows.Forms.Label()
        Me.cbTipoDeRecipiente1 = New System.Windows.Forms.ComboBox()
        Me.lblTipoDeRecipiente1 = New System.Windows.Forms.Label()
        Me.dtpFechaInicio1 = New System.Windows.Forms.DateTimePicker()
        Me.lblFechaInicio1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'dtpFechaFin
        '
        Me.dtpFechaFin.Location = New System.Drawing.Point(285, 301)
        Me.dtpFechaFin.Name = "dtpFechaFin"
        Me.dtpFechaFin.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaFin.TabIndex = 44
        '
        'lblFechaFin
        '
        Me.lblFechaFin.AutoSize = True
        Me.lblFechaFin.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaFin.Location = New System.Drawing.Point(145, 301)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(83, 19)
        Me.lblFechaFin.TabIndex = 43
        Me.lblFechaFin.Text = "Fecha de fin"
        '
        'dtpFechaInicio
        '
        Me.dtpFechaInicio.Location = New System.Drawing.Point(285, 275)
        Me.dtpFechaInicio.Name = "dtpFechaInicio"
        Me.dtpFechaInicio.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaInicio.TabIndex = 42
        '
        'lblFechaInicio
        '
        Me.lblFechaInicio.AutoSize = True
        Me.lblFechaInicio.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaInicio.Location = New System.Drawing.Point(145, 275)
        Me.lblFechaInicio.Name = "lblFechaInicio"
        Me.lblFechaInicio.Size = New System.Drawing.Size(100, 19)
        Me.lblFechaInicio.TabIndex = 41
        Me.lblFechaInicio.Text = "Fecha de inicio"
        '
        'txtCantidad
        '
        Me.txtCantidad.Location = New System.Drawing.Point(285, 246)
        Me.txtCantidad.Name = "txtCantidad"
        Me.txtCantidad.Size = New System.Drawing.Size(120, 20)
        Me.txtCantidad.TabIndex = 40
        '
        'lblCantidad
        '
        Me.lblCantidad.AutoSize = True
        Me.lblCantidad.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCantidad.Location = New System.Drawing.Point(145, 245)
        Me.lblCantidad.Name = "lblCantidad"
        Me.lblCantidad.Size = New System.Drawing.Size(64, 19)
        Me.lblCantidad.TabIndex = 39
        Me.lblCantidad.Text = "Cantidad"
        '
        'cbUvas
        '
        Me.cbUvas.FormattingEnabled = True
        Me.cbUvas.Location = New System.Drawing.Point(284, 133)
        Me.cbUvas.Name = "cbUvas"
        Me.cbUvas.Size = New System.Drawing.Size(121, 21)
        Me.cbUvas.TabIndex = 38
        '
        'lblUvas
        '
        Me.lblUvas.AutoSize = True
        Me.lblUvas.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUvas.Location = New System.Drawing.Point(145, 132)
        Me.lblUvas.Name = "lblUvas"
        Me.lblUvas.Size = New System.Drawing.Size(80, 19)
        Me.lblUvas.TabIndex = 37
        Me.lblUvas.Text = "Tipo de uva"
        '
        'txtLote
        '
        Me.txtLote.Location = New System.Drawing.Point(285, 218)
        Me.txtLote.Name = "txtLote"
        Me.txtLote.Size = New System.Drawing.Size(120, 20)
        Me.txtLote.TabIndex = 36
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(285, 190)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbIdRecipiente.TabIndex = 35
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(145, 189)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(84, 19)
        Me.lblIdRecipiente.TabIndex = 34
        Me.lblIdRecipiente.Text = "Id recipiente"
        '
        'lblLote
        '
        Me.lblLote.AutoSize = True
        Me.lblLote.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLote.Location = New System.Drawing.Point(145, 217)
        Me.lblLote.Name = "lblLote"
        Me.lblLote.Size = New System.Drawing.Size(37, 19)
        Me.lblLote.TabIndex = 33
        Me.lblLote.Text = "Lote"
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(117, 335)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 46
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(467, 335)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 45
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'cbIdRecipiente1
        '
        Me.cbIdRecipiente1.FormattingEnabled = True
        Me.cbIdRecipiente1.Location = New System.Drawing.Point(285, 50)
        Me.cbIdRecipiente1.Name = "cbIdRecipiente1"
        Me.cbIdRecipiente1.Size = New System.Drawing.Size(121, 21)
        Me.cbIdRecipiente1.TabIndex = 48
        '
        'lblIngresar1
        '
        Me.lblIngresar1.AutoSize = True
        Me.lblIngresar1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIngresar1.Location = New System.Drawing.Point(145, 49)
        Me.lblIngresar1.Name = "lblIngresar1"
        Me.lblIngresar1.Size = New System.Drawing.Size(84, 19)
        Me.lblIngresar1.TabIndex = 47
        Me.lblIngresar1.Text = "Id recipiente"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(507, 83)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 49
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'cbTipoDeRecipiente
        '
        Me.cbTipoDeRecipiente.FormattingEnabled = True
        Me.cbTipoDeRecipiente.Location = New System.Drawing.Point(284, 161)
        Me.cbTipoDeRecipiente.Name = "cbTipoDeRecipiente"
        Me.cbTipoDeRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbTipoDeRecipiente.TabIndex = 51
        '
        'lblTipoDeRecipiente
        '
        Me.lblTipoDeRecipiente.AutoSize = True
        Me.lblTipoDeRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente.Location = New System.Drawing.Point(145, 161)
        Me.lblTipoDeRecipiente.Name = "lblTipoDeRecipiente"
        Me.lblTipoDeRecipiente.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente.TabIndex = 50
        Me.lblTipoDeRecipiente.Text = "Tipo de recipiente"
        '
        'cbTipoDeRecipiente1
        '
        Me.cbTipoDeRecipiente1.FormattingEnabled = True
        Me.cbTipoDeRecipiente1.Location = New System.Drawing.Point(285, 20)
        Me.cbTipoDeRecipiente1.Name = "cbTipoDeRecipiente1"
        Me.cbTipoDeRecipiente1.Size = New System.Drawing.Size(121, 21)
        Me.cbTipoDeRecipiente1.TabIndex = 53
        '
        'lblTipoDeRecipiente1
        '
        Me.lblTipoDeRecipiente1.AutoSize = True
        Me.lblTipoDeRecipiente1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente1.Location = New System.Drawing.Point(145, 20)
        Me.lblTipoDeRecipiente1.Name = "lblTipoDeRecipiente1"
        Me.lblTipoDeRecipiente1.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente1.TabIndex = 52
        Me.lblTipoDeRecipiente1.Text = "Tipo de recipiente"
        '
        'dtpFechaInicio1
        '
        Me.dtpFechaInicio1.Location = New System.Drawing.Point(284, 86)
        Me.dtpFechaInicio1.Name = "dtpFechaInicio1"
        Me.dtpFechaInicio1.Size = New System.Drawing.Size(200, 20)
        Me.dtpFechaInicio1.TabIndex = 55
        '
        'lblFechaInicio1
        '
        Me.lblFechaInicio1.AutoSize = True
        Me.lblFechaInicio1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaInicio1.Location = New System.Drawing.Point(144, 86)
        Me.lblFechaInicio1.Name = "lblFechaInicio1"
        Me.lblFechaInicio1.Size = New System.Drawing.Size(100, 19)
        Me.lblFechaInicio1.TabIndex = 54
        Me.lblFechaInicio1.Text = "Fecha de inicio"
        '
        'frmModificacionProducto
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.dtpFechaInicio1)
        Me.Controls.Add(Me.lblFechaInicio1)
        Me.Controls.Add(Me.cbTipoDeRecipiente1)
        Me.Controls.Add(Me.lblTipoDeRecipiente1)
        Me.Controls.Add(Me.cbTipoDeRecipiente)
        Me.Controls.Add(Me.lblTipoDeRecipiente)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.cbIdRecipiente1)
        Me.Controls.Add(Me.lblIngresar1)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.dtpFechaFin)
        Me.Controls.Add(Me.lblFechaFin)
        Me.Controls.Add(Me.dtpFechaInicio)
        Me.Controls.Add(Me.lblFechaInicio)
        Me.Controls.Add(Me.txtCantidad)
        Me.Controls.Add(Me.lblCantidad)
        Me.Controls.Add(Me.cbUvas)
        Me.Controls.Add(Me.lblUvas)
        Me.Controls.Add(Me.txtLote)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.lblLote)
        Me.Name = "frmModificacionProducto"
        Me.Text = "Modificaciones de Productos"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dtpFechaFin As DateTimePicker
    Friend WithEvents lblFechaFin As Label
    Friend WithEvents dtpFechaInicio As DateTimePicker
    Friend WithEvents lblFechaInicio As Label
    Friend WithEvents txtCantidad As TextBox
    Friend WithEvents lblCantidad As Label
    Friend WithEvents cbUvas As ComboBox
    Friend WithEvents lblUvas As Label
    Friend WithEvents txtLote As TextBox
    Friend WithEvents cbIdRecipiente As ComboBox
    Friend WithEvents lblIdRecipiente As Label
    Friend WithEvents lblLote As Label
    Friend WithEvents btnVolver As Button
    Friend WithEvents btnIngresar As Button
    Friend WithEvents cbIdRecipiente1 As ComboBox
    Friend WithEvents lblIngresar1 As Label
    Friend WithEvents btnBuscar As Button
    Friend WithEvents cbTipoDeRecipiente As ComboBox
    Friend WithEvents lblTipoDeRecipiente As Label
    Friend WithEvents cbTipoDeRecipiente1 As ComboBox
    Friend WithEvents lblTipoDeRecipiente1 As Label
    Friend WithEvents dtpFechaInicio1 As DateTimePicker
    Friend WithEvents lblFechaInicio1 As Label
End Class
