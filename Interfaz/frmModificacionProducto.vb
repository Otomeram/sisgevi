﻿Public Class frmModificacionProducto
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmModificaciones.Show()
        Dispose()
    End Sub

    Private Sub frmModificacionProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbUvas, "nombre", "Producto")
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
        fill(cbTipoDeRecipiente1, "tipo", "Recipiente")
        btnIngresar.Enabled = False
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        sql = "SELECT * FROM Va WHERE id_recipiente = '" & cbIdRecipiente1.SelectedItem.ToString() & "' AND fecha = '" & dtpFechaInicio1.Value.ToString("dd/MM/yyyy") & "'"
        rs.Open(sql, cn)
        If rs.RecordCount = 0 Then
            MsgBox("No se han encontrado resultados coincidentes con los parámetros de búsqueda ingresados")
        Else
            cbUvas.SelectedItem = rs("nombre").Value
            cbIdRecipiente.SelectedItem = rs("id_recipiente").Value
            txtLote.Text = rs("id").Value
            txtCantidad.Text = rs("cantidad").Value
            dtpFechaInicio.Text = rs("fecha_inicio").Value
            dtpFechaFin.Text = rs("fecha_fin").Value
            If rs.State = 1 Then
                rs.Close()
            End If
            sql = "SELECT tipo FROM Recipiente WHERE id_recipiente = '" & rs("id_recipiente").Value & "'"
            rs.Open(sql, cn)
            cbTipoDeRecipiente.SelectedItem = rs(0).Value
            btnIngresar.Enabled = True
        End If
        If rs.State = 1 Then
            rs.Close()
        End If
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = txtCantidad.Text.Any And txtLote.Text.Any
        validInput = IsNumeric(txtCantidad.Text) And IsNumeric(txtLote.Text)
        If validInput And fullFields Then
            sql = "UPDATE Va SET id_recipiente = '" & cbIdRecipiente.SelectedItem.ToString() & "', nombre = '" & cbUvas.SelectedItem.ToString() & "', fecha_inicio = '" & dtpFechaInicio.Value.ToString("dd/MM/yyyy") & "', fecha_fin = '" & dtpFechaFin.Value.ToString("dd/MM/yyyy") & "', lote = '" & txtLote.Text & "', cantidad = '" & txtCantidad.Text & " WHERE id_recipiente = '" & cbIdRecipiente1.SelectedItem.ToString() & "' AND fecha = '" & dtpFechaInicio1.Value.ToString("dd/MM/yyyy") & "'"
            cn.Execute(sql)
            MsgBox("La modificación fue realizada exitosamente")
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("Uno o más campos numéricos contienen caracteres inválidos")
        End If
    End Sub

    Private Sub cbTipoDeRecipiente1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente1.SelectedIndexChanged
        updateId(cbIdRecipiente1, cbTipoDeRecipiente1.Text)
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class
