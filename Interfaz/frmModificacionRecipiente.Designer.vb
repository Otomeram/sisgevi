﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificacionRecipiente
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtCapacidad = New System.Windows.Forms.TextBox()
        Me.lblCapacidad = New System.Windows.Forms.Label()
        Me.cbTipo = New System.Windows.Forms.ComboBox()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.btnIngresar = New System.Windows.Forms.Button()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.lblIdRecipiente = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.cbIdRecipiente = New System.Windows.Forms.ComboBox()
        Me.cbTipoDeRecipiente = New System.Windows.Forms.ComboBox()
        Me.lblTipoDeRecipiente = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'txtCapacidad
        '
        Me.txtCapacidad.Location = New System.Drawing.Point(334, 200)
        Me.txtCapacidad.Name = "txtCapacidad"
        Me.txtCapacidad.Size = New System.Drawing.Size(100, 20)
        Me.txtCapacidad.TabIndex = 28
        '
        'lblCapacidad
        '
        Me.lblCapacidad.AutoSize = True
        Me.lblCapacidad.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCapacidad.Location = New System.Drawing.Point(145, 199)
        Me.lblCapacidad.Name = "lblCapacidad"
        Me.lblCapacidad.Size = New System.Drawing.Size(136, 19)
        Me.lblCapacidad.TabIndex = 27
        Me.lblCapacidad.Text = "Capacidad (en litros)"
        '
        'cbTipo
        '
        Me.cbTipo.FormattingEnabled = True
        Me.cbTipo.Location = New System.Drawing.Point(334, 161)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Size = New System.Drawing.Size(151, 21)
        Me.cbTipo.TabIndex = 26
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(96, 294)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 25
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'btnIngresar
        '
        Me.btnIngresar.Location = New System.Drawing.Point(446, 294)
        Me.btnIngresar.Name = "btnIngresar"
        Me.btnIngresar.Size = New System.Drawing.Size(75, 23)
        Me.btnIngresar.TabIndex = 24
        Me.btnIngresar.Text = "Ingresar"
        Me.btnIngresar.UseVisualStyleBackColor = True
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipo.Location = New System.Drawing.Point(145, 164)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(36, 19)
        Me.lblTipo.TabIndex = 23
        Me.lblTipo.Text = "Tipo"
        '
        'lblIdRecipiente
        '
        Me.lblIdRecipiente.AutoSize = True
        Me.lblIdRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIdRecipiente.Location = New System.Drawing.Point(145, 87)
        Me.lblIdRecipiente.Name = "lblIdRecipiente"
        Me.lblIdRecipiente.Size = New System.Drawing.Size(106, 19)
        Me.lblIdRecipiente.TabIndex = 29
        Me.lblIdRecipiente.Text = "Id del recipiente"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(462, 88)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(75, 23)
        Me.btnBuscar.TabIndex = 31
        Me.btnBuscar.Text = "Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'cbIdRecipiente
        '
        Me.cbIdRecipiente.FormattingEnabled = True
        Me.cbIdRecipiente.Location = New System.Drawing.Point(334, 88)
        Me.cbIdRecipiente.Name = "cbIdRecipiente"
        Me.cbIdRecipiente.Size = New System.Drawing.Size(74, 21)
        Me.cbIdRecipiente.TabIndex = 32
        '
        'cbTipoDeRecipiente
        '
        Me.cbTipoDeRecipiente.FormattingEnabled = True
        Me.cbTipoDeRecipiente.Location = New System.Drawing.Point(334, 61)
        Me.cbTipoDeRecipiente.Name = "cbTipoDeRecipiente"
        Me.cbTipoDeRecipiente.Size = New System.Drawing.Size(121, 21)
        Me.cbTipoDeRecipiente.TabIndex = 34
        '
        'lblTipoDeRecipiente
        '
        Me.lblTipoDeRecipiente.AutoSize = True
        Me.lblTipoDeRecipiente.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoDeRecipiente.Location = New System.Drawing.Point(145, 57)
        Me.lblTipoDeRecipiente.Name = "lblTipoDeRecipiente"
        Me.lblTipoDeRecipiente.Size = New System.Drawing.Size(117, 19)
        Me.lblTipoDeRecipiente.TabIndex = 33
        Me.lblTipoDeRecipiente.Text = "Tipo de recipiente"
        '
        'frmModificacionRecipiente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.cbTipoDeRecipiente)
        Me.Controls.Add(Me.lblTipoDeRecipiente)
        Me.Controls.Add(Me.cbIdRecipiente)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.lblIdRecipiente)
        Me.Controls.Add(Me.txtCapacidad)
        Me.Controls.Add(Me.lblCapacidad)
        Me.Controls.Add(Me.cbTipo)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnIngresar)
        Me.Controls.Add(Me.lblTipo)
        Me.Name = "frmModificacionRecipiente"
        Me.Text = "Modificaciones de Recipientes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtCapacidad As TextBox
    Friend WithEvents lblCapacidad As Label
    Friend WithEvents cbTipo As ComboBox
    Friend WithEvents btnVolver As Button
    Friend WithEvents btnIngresar As Button
    Friend WithEvents lblTipo As Label
    Friend WithEvents lblIdRecipiente As Label
    Friend WithEvents btnBuscar As Button
    Friend WithEvents cbIdRecipiente As ComboBox
    Friend WithEvents cbTipoDeRecipiente As ComboBox
    Friend WithEvents lblTipoDeRecipiente As Label
End Class
