﻿Public Class frmModificacionRecipiente
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmModificaciones.Show()
        Dispose()
    End Sub

    Private Sub frmModificacionRecipiente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
        fill(cbTipo, "tipo", "Recipiente")
        btnIngresar.Enabled = False
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        sql = "SELECT * FROM Recipiente WHERE id_recipiente = '" & cbIdRecipiente.SelectedItem.ToString() & "'"
        rs.Open(sql, cn)
        cbTipo.SelectedItem = rs("tipo").Value
        txtCapacidad.Text = rs("capacidad").Value
        If rs.State = 1 Then
            rs.Close()
        End If
        btnIngresar.Enabled = True
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.SelectedItem.ToString())
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        If txtCapacidad.Text.Any And IsNumeric(txtCapacidad.Text) Then
            sql = "UPDATE Recipiente SET tipo = '" & cbTipo.SelectedItem.ToString() & "', capacidad = '" & txtCapacidad.Text & "' WHERE id_recipiente = '" & cbIdRecipiente.SelectedItem.ToString() & "'"
            cn.Execute(sql)
            MsgBox("La modificación fue realizada exitosamente")
        ElseIf Not txtCapacidad.Text.Any Then
            MsgBox("El campo Capacidad es obligatorio")
        ElseIf Not IsNumeric(txtCapacidad.Text) Then
            MsgBox("El campo Capacidad contiene caracteres inválidos")
        End If
    End Sub
End Class