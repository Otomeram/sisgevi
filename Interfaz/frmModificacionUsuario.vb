﻿Public Class frmModificacionUsuario
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmUsuarios.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        fullFields = txtApellido.Text.Any And txtCalle.Text.Any And txtEsquina.Text.Any And txtNombre.Text.Any And txtNPuerta.Text.Any And cbTel.Items.Count <> 0
        validInput = IsNumeric(txtNPuerta.Text)
        If fullFields And validInput Then
            cn.Execute(sql)
            sql = "UPDATE Usuario SET apellido = '" & txtApellido.Text & "', calle = '" & txtCalle.Text & ", esquina = " & txtEsquina.Text & "', nombre = '" & txtNombre.Text & "', n_puerta = '" & txtNPuerta.Text & "', fecha_nacimiento = '" & dtpFechaDeNacimiento.Value.ToString("dd/MM/yyyy") & "', rol = '" & cbRol.SelectedItem.ToString & "' WHERE ci = '" & txtCI.Text & "'"
            sql = "DELETE FROM TelefonoUsuario WHERE ci = '" & txtCI.Text & "'"
            cn.Execute(sql)
            For Each tel In cbTel.Items
                sql = "INSERT INTO TelefonoUsuario VALUES('" & txtCI.Text & "', '" & tel & "')"
                cn.Execute(sql)
            Next
            MsgBox("El Usuario fue modificado exitosamente")
        ElseIf Not fullFields Then
            MsgBox("Todos los campos son obligatorios")
        ElseIf Not validInput Then
            MsgBox("El número de puerta ingresado contiene caracteres inválidos")
        End If
    End Sub

    Private Sub frmModificacionUsuario_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbRol)
        If Not ci = "12345678" Then
            cbRol.Items.Remove("Gerente")
        End If
        btnIngresar.Enabled = False
    End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        If txtCI.Text.Any And IsNumeric(txtCI.Text) And Not noRegistrado(txtCI.Text) Then
            loadInfo()
            btnIngresar.Enabled = True
        ElseIf noRegistrado(txtCI.Text) Then
            MsgBox("El usuario ingresado no está registrado")
        ElseIf Not txtCI.Text.Any Then
            MsgBox("El campo CI es obligatorio")
        ElseIf Not IsNumeric(txtCI.Text) Then
            MsgBox("El campo CI contiene caracteres inválidos")
        End If
    End Sub

    Private Sub loadInfo()
        sql = "SELECT * FROM Usuario WHERE ci = '" & txtCI.Text & "'"
        rs.Open(sql, cn)
        txtApellido.Text = rs("apellido").Value
        txtCalle.Text = rs("calle").Value
        txtEsquina.Text = rs("esquina").Value
        txtNombre.Text = rs("nombre").Value
        txtNPuerta.Text = rs("n_puerta").Value
        dtpFechaDeNacimiento.Value = Date.Parse(rs("fecha_nacimiento").Value)
        cbRol.SelectedItem = rs("rol").Value
        rs.Close()
        sql = "SELECT telefono FROM TelefonoUsuario WHERE ci = '" & txtCI.Text & "'"
        rs.Open(sql, cn)
        Do Until rs.EOF
            cbTel.Items.Add(rs("telefono").Value)
            rs.MoveNext()
        Loop
        rs.Close()
    End Sub

    Private Sub btnAddTel_Click(sender As Object, e As EventArgs) Handles btnAddTel.Click
        If IsNumeric(txtTel.Text) Then
            addItem(cbTel, txtTel)
        Else
            MsgBox("El teléfono ingresado contiene caracteres inválidos")
        End If
    End Sub

    Private Sub btnRemoveTel_Click(sender As Object, e As EventArgs) Handles btnRemoveTel.Click
        cbTel.Items.Remove(cbTel.SelectedItem)
    End Sub
End Class