﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModificaciones
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnProceso = New System.Windows.Forms.Button()
        Me.btnRecipiente = New System.Windows.Forms.Button()
        Me.btnProducto = New System.Windows.Forms.Button()
        Me.btnVolver = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnProceso
        '
        Me.btnProceso.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProceso.Location = New System.Drawing.Point(187, 199)
        Me.btnProceso.Name = "btnProceso"
        Me.btnProceso.Size = New System.Drawing.Size(231, 66)
        Me.btnProceso.TabIndex = 9
        Me.btnProceso.Text = "Proceso"
        Me.btnProceso.UseVisualStyleBackColor = True
        '
        'btnRecipiente
        '
        Me.btnRecipiente.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRecipiente.Location = New System.Drawing.Point(338, 84)
        Me.btnRecipiente.Name = "btnRecipiente"
        Me.btnRecipiente.Size = New System.Drawing.Size(231, 66)
        Me.btnRecipiente.TabIndex = 7
        Me.btnRecipiente.Text = "Recipiente"
        Me.btnRecipiente.UseVisualStyleBackColor = True
        '
        'btnProducto
        '
        Me.btnProducto.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProducto.Location = New System.Drawing.Point(45, 84)
        Me.btnProducto.Name = "btnProducto"
        Me.btnProducto.Size = New System.Drawing.Size(231, 66)
        Me.btnProducto.TabIndex = 6
        Me.btnProducto.Text = "Producto"
        Me.btnProducto.UseVisualStyleBackColor = True
        '
        'btnVolver
        '
        Me.btnVolver.Location = New System.Drawing.Point(23, 320)
        Me.btnVolver.Name = "btnVolver"
        Me.btnVolver.Size = New System.Drawing.Size(75, 23)
        Me.btnVolver.TabIndex = 24
        Me.btnVolver.Text = "Volver"
        Me.btnVolver.UseVisualStyleBackColor = True
        '
        'frmModificaciones
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(617, 370)
        Me.Controls.Add(Me.btnVolver)
        Me.Controls.Add(Me.btnProceso)
        Me.Controls.Add(Me.btnRecipiente)
        Me.Controls.Add(Me.btnProducto)
        Me.Name = "frmModificaciones"
        Me.Text = "Modificaciones"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnProceso As Button
    Friend WithEvents btnUva As Button
    Friend WithEvents btnRecipiente As Button
    Friend WithEvents btnProducto As Button
    Friend WithEvents btnVolver As Button
End Class
