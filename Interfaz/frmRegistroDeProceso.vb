﻿Public Class frmRegistroDeProceso
    Private Sub btnVolver_Click(sender As Object, e As EventArgs) Handles btnVolver.Click
        frmIngresos.Show()
        Dispose()
    End Sub

    Private Sub btnIngresar_Click(sender As Object, e As EventArgs) Handles btnIngresar.Click
        sql = "INSERT INTO Pasa VALUES('" & cbIdRecipiente.Text & "', '" & cbNombreDelProceso.Text & "', '" & dtpFechaInicio.Value.ToString("dd/MM/yyyy") & "', '" & dtpFechaFin.Value.ToString("dd/MM/yyyy") & "')"
        cn.Execute(sql)
    End Sub

    Private Sub frmRegistroDeProceso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        fill(cbNombreDelProceso, "nombre", "Proceso")
        fill(cbTipoDeRecipiente, "tipo", "Recipiente")
    End Sub

    Private Sub cbTipoDeRecipiente_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbTipoDeRecipiente.SelectedIndexChanged
        updateId(cbIdRecipiente, cbTipoDeRecipiente.Text)
    End Sub
End Class